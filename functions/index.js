//import * as functions from 'firebase-functions';
//import * as admin from 'firebase-admin';
const functions = require('firebase-functions')
const admin = require('firebase-admin')
const cors = require('cors')({origin: true})
const corser = require('cors')
const corsHandler = corser({origin: true})
const env = functions.config()
const moment = require('moment')
moment.locale('es')

//const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG)
const SENDGRID_API_KEY = env.sendgrid.key


admin.initializeApp(functions.config().firebase)

const sgMail = require('@sendgrid/mail')
      sgMail.setApiKey(SENDGRID_API_KEY)
      sgMail.setSubstitutionWrappers("{{", "}}")

const SGfrom = {
  "email": "no-reply@indolora.es",
  "name": "Indolora"
}

// 1.1 - Admin recibe cita sin confirmar
exports.nuevaCitaAdminMail = functions.firestore
  .document('citas/{citaId}') //any write to this node will trigger email
  .onCreate((doc) => {
    const cita = doc.data()

    //return admin.auth().getUserByEmail(especialista.email)

    return admin.auth().listUsers(1000)
      .then(function(listUsersResult) {
        listUsersResult.users.forEach(function(userRecord) {
          if ( userRecord.customClaims.role !== 'admin' ) return

          // Unimos especialistas en un string
          let espis = ''
          if(cita.slotDetails.length > 1) {
            cita.slotDetails.forEach(record => espis = espis + record.especialista + ', ')
          } else {
            espis = cita.slotDetails[0].especialista
          }


          //direccion por ahora
          const direccion = "Direccion Falsa, 123, Pernambuco"

          const msg = {
            to: userRecord.email,
            from: SGfrom,
            subject:  'Nueva cita - Por confirmar',
            // text: `Hey ${toName}. You have a new follower!!! `,
            // html: `<strong>Hey ${toName}. You have a new follower!!!</strong>`,

            // custom templates
            templateId: 'd-2e05231fe10e4301bd0ea3d706a1f73f',
            dynamicTemplateData: {
              Sender_Name: "Hello",
              clienteName: cita.cliente.displayName,
              clienteDireccion: direccion,
              pacienteName: cita.detalles.nombre_paciente,
              procedimiento: cita.detalles.procedimiento,
              fechaYHora: moment(cita.detalles.start).locale('es').format('LLLL'),
              especialistas: espis
            }
          }

          return sgMail.send(msg)
        })
      })
      .then(() => console.log('emails sent!'))
      .catch(function(error) {
        console.log("Error listing users:", error);
      });

    // Admin mailing
  })

// 1.2: Especialista recibe cita para confirmar
exports.nuevaCitaParaTodos = functions.https.onCall(
  (cita) => {

    // Mailing para el cliente
    const msgCliente = {
      to: cita.cliente.email,
      from: SGfrom,
      templateId: 'd-767c3c7c4d2f4c05a20505d456c36e9c',
      dynamicTemplateData: {
        citaId: cita.id,
        clienteName: cita.cliente.displayName,
        pacienteName: cita.detalles.nombre_paciente,
        procedimiento: cita.detalles.procedimiento,
        duracion: cita.detalles.duracion,
        fechaYHora: moment(cita.detalles.start).locale('es').format('LLLL'),
      }
    }

    sgMail.send(msgCliente)
        .then(() => console.log('email sent!'))
        .catch(err => console.log(err) )

    // Mailing  par todos los especialistas
    cita.slotDetails.forEach(async ({especialista}) => {
      const msg = {
        to: especialista,
        from: SGfrom,
        templateId: 'd-c952edff34f7469a807e7c4d48c0a3bc',
        dynamicTemplateData: {
          citaId: cita.id,
          clienteName: cita.cliente.displayName,
          clienteDireccion: cita.cliente.address,
          pacienteName: cita.detalles.nombre_paciente,
          procedimiento: cita.detalles.procedimiento,
          duracion: cita.detalles.duracion,
          fechaYHora: moment(cita.detalles.start).locale('es').format('LLLL'),
        }
      }

      sgMail.send(msg)
        .then(() => console.log('email sent!'))
        .catch(err => console.log(err) )
    })

  }
)

// 2: Admin recibe confirmacion, por parte de los especialistas
exports.citaConfirmadaParaAdmin = functions.https.onCall(
  ({cita, email}, context) => {
    return admin.auth().listUsers(1000)
      .then(function(listUsersResult) {
        listUsersResult.users.forEach(function(userRecord) {
          // Buscamos todos los admins
          if ( !userRecord.customClaims || !userRecord.customClaims.role.toLowerCase().includes('admin') ) return

          console.log(userRecord)

          const msg = {
            to: userRecord.email,
            from: 'no-reply@indolora.es',

            // custom templates
            templateId: 'd-683d36155b67404299e7ee4a90c82aae',
            dynamicTemplateData: {
              id: cita.id,
              especialista: email,
              clienteName: cita.cliente.displayName,
              pacienteName: cita.detalles.nombre_paciente,
              procedimiento: cita.detalles.procedimiento,
              fechaYHora: moment(cita.detalles.start).locale('es').format('LLLL'),
              quienConfirma: email
            }
          }
          console.log('Sending email to ' + userRecord.email)
          return sgMail.send(msg)
        })
      })
      .then(() => console.log('emails sent!'))
      .catch(function(error) {
        console.log("Error listing users:", error);
      });
  }
)

// 3: Admin valida
exports.citaValidadaPorAdmin = functions.https.onCall(
  (cita, context) => {
    if ( !context.auth.token.role.includes('admin') ) return { error: 'Solo admins, listillo' }
    const templateId = !cita.autorizada.porAdmin ? 'd-770f48b80b0c479a81e8a86ea720c8bf' : 'd-7ce2d9e8abc04868a5e2bf985f23e7f5'
    console.log(`CitaAutorizadaByAdminQueLlegaAFuncion: ${cita.autorizada.porAdmin} | Template usado: ${templateId}`)

    const cliente = cita.cliente.email
    const msg = {
      to: cliente,
      from: 'no-reply@indolora.es',
      templateId,
      dynamicTemplateData: {
        id: cita.id,
        clienteName: cita.cliente.displayName,
        pacienteName: cita.detalles.nombre_paciente,
        procedimiento: cita.detalles.procedimiento,
        fechaYHora: moment(cita.detalles.start).locale('es').format('LLLL'),
      }
    }

    sgMail.send(msg)
      .then(() => console.log('Email enviado al cliente!'))
      .catch(function(error) {
        console.log("Error enviando email al cliente:", error);
      })
  }
)

// 4: Despues de validar, cliente confirma
exports.citaConfirmadaPorClienteParaAdmin = functions.https.onCall(
  (cita, context) => {
    return admin.auth().listUsers(1000)
      .then(function(listUsersResult) {
        listUsersResult.users.forEach(function(userRecord) {
          if ( userRecord.customClaims.role !== 'admin' ) return;

          const msg = {
            to: userRecord.email,
            from: 'no-reply@indolora.es',

            // custom templates
            templateId: 'd-c974de9e57b04577a844118cd25b661b',
            dynamicTemplateData: {
              id: cita.id,
              especialista: cita.autorizada.especialista,
              clienteName: cita.cliente.displayName,
              pacienteName: cita.detalles.nombre_paciente,
              procedimiento: cita.detalles.procedimiento,
              fechaYHora: moment(cita.detalles.start).locale('es').format('LLLL'),
              quienConfirma: cita.cliente.displayName
            }
          }

          return sgMail.send(msg)
        })
      })
      .then(() => console.log('emails sent!'))
      .catch(function(error) {
        console.log("Error listing users:", error);
      });
  }
)
exports.citaConfirmadaPorClienteParaEspis = functions.https.onCall(
  (cita, context) => {
    console.log(cita)
    // Mailing para el cliente
    const msgEspi = {
      to: cita.autorizada.especialista,
      from: 'no-reply@indolora.es',
      templateId: 'd-5244977066f3424f93e6088dfed50b4a',
      dynamicTemplateData: {
        citaId: cita.id,
        clienteName: cita.cliente.displayName,
        pacienteName: cita.detalles.nombre_paciente,
        procedimiento: cita.detalles.procedimiento,
        duracion: cita.detalles.duracion,
        fechaYHora: moment(cita.detalles.start).locale('es').format('LLLL'),
      }
    }

    sgMail.send(msgEspi)
      .then(() => console.log('email sent!'))
      .catch(err => console.log(err) )
  }
)

// Otros
exports.invitarEspecialista = functions.https.onCall(
  (data, context) => {
    // check request is made by an admin

    if ( context.auth.token.role !== 'admin' ) {
      return { error: 'Solo admins, listillo' }
    }

    const msg = {
      to: data.email,
      from: 'no-reply@indolora.es',
      subject:  'Bienvenid@, especialista!',
      // custom templates
      templateId: 'd-407d6f5e7562450d84e949441b365432',

      dynamicTemplateData: {
        email: data.email
      }
    }

    sgMail.send(msg)
      .then(() => console.log('Enviado mail a nuevo especialista!'))
      .catch(err => console.log(err))
  }
);


/* const Airtable = require('airtable')
const base = new Airtable({apiKey: env.airtable.key}).base('appThdeODtEZSjum8')

exports.addToAirtable = functions.firestore
  .document('users/{userId}')
  .onCreate((snap, context) => {
    const data = snap.data()

    corsHandler(request, response, () => {
      response.status(200).send('Data: ' + data );
    })
}) */



// Primera: añadir claims
exports.setRole = functions.https.onCall((data, context) => {
  // check request is made by an admin

  if ( context.auth.token.role === data.role ) {
    console.log('already exists')
    return { itsOK: 'We already got you baby' }
  }

  // get user and add admin custom claim
  return admin.auth().getUserByEmail(data.email).then(user => {
    return admin.auth().setCustomUserClaims(user.uid, {role: data.role})
  }).then(() => {
    console.log(data.role, `Éxito! ${data.email} es ahora un ${data.role}.`)
    return {
      status: 'success',
      message: `Éxito! ${data.email} es ahora un ${data.role}.`
    }
  }).catch(err => {
    console.log(err)
    return err.message;
  });
});

exports.makeMeAdmin = functions.https.onCall((data, context) => {
  // check request is made by an admin

  /* if ( context.auth.token.role !== 'admin' ) {
    return { error: 'Only admins can add other admins' }
  } */

  // get user and add admin custom claim
  return admin.auth().getUserByEmail(data.email).then(user => {
    return admin.auth().setCustomUserClaims(user.uid, {
      role: 'admin'
    })
  }).then(() => {
    return {
      message: `Éxito! ${data.email} es ahora un ADMIN.`
    }
  }).catch(err => {
    return err;
  });
});
