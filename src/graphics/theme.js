import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      // light: will be calculated from palette.primary.main,
      main: '#1E656D',
      // dark: will be calculated from palette.primary.main,
      // contrastText: will be calculated to contrast with palette.primary.main
    },
    secondary: {
      //light: '#0066ff',
      main: '#832609',

      // dark: will be calculated from palette.secondary.main,
      //contrastText: '#ffcc00',
    },
    error: {
      main: '#e51a4c'
    }
  },
  typography: {
    useNextVariants: true,
  }
})

export default theme
