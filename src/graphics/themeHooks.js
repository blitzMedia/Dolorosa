const themeHooks = {
  background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
  zIndex: {
    drawer: 1200
  },
  spacing: {
    unit: 8
  },
  elements: {
    drawerWidth: 240,
  }
}

export default themeHooks
