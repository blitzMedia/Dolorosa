import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/functions'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyDSyaO3Gi4CT2JJW0DQwAn8UcPPV_0gpEs",
  authDomain: "dolorosa-9eb85.firebaseapp.com",
  databaseURL: "https://dolorosa-9eb85.firebaseio.com",
  projectId: "dolorosa-9eb85",
  storageBucket: "dolorosa-9eb85.appspot.com",
  messagingSenderId: "255960877281",
  clientId: "255960877281-s9ipfmscv3km1797stp265089bj9v4t2.apps.googleusercontent.com",
  clientSecret: "ZywmwP3TSoa0pFZAfOTE9Qnb",
  scopes: [
    "email",
    "profile",
    "https://www.googleapis.com/auth/calendar"
  ],
  discoveryDocs: [
  "https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest"
  ]
}

firebase.initializeApp(config)
firebase.firestore().settings({ timestampsInSnapshots: true })

export default firebase
