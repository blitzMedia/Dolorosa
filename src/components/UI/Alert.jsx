import React from 'react'
import SweetAlert from 'react-bootstrap-sweetalert'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { unsetAlert } from '../../store/actions/otherActions'
import withStyles from '@material-ui/core/styles/withStyles'
import buttonStyle from '../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/components/buttonStyle.jsx'

export class Alert extends React.Component {

  destroyAlert = () => {
    this.props.unsetAlert(null)
  }

  render() {
    if(this.props.alert) {

      const { title = '', text, status } = this.props.alert
      return(
        <SweetAlert
          type={status}
          title={title}
          onConfirm={this.destroyAlert}
          confirmBtnCssClass={ this.props.classes.button + " " + this.props.classes.success }
          cancelBtnCssClass={ this.props.classes.button + " " + this.props.classes.danger }
        >
          {text}
        </SweetAlert>
      )
    } else {
      return null
    }
  }
}

//redux container component
const mapStateToProps = (state, ownProps) => {
  return({ alert: state.UI.alert })
}

const mapDispatchToProps = (dispatch) => {
  return {
    unsetAlert: (alert) => dispatch(unsetAlert(alert))
  }
}

export const HandleAlert = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(buttonStyle)
)(Alert)

export default Alert
