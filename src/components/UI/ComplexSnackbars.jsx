import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import InfoIcon from '@material-ui/icons/Info'
import CloseIcon from '@material-ui/icons/Close'
import green from '@material-ui/core/colors/green'
import amber from '@material-ui/core/colors/amber'
import IconButton from '@material-ui/core/IconButton'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import WarningIcon from '@material-ui/icons/Warning'
import { withStyles } from '@material-ui/core/styles'

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const styles1 = theme => ({
  success: { backgroundColor: green[600], },
  error: { backgroundColor: theme.palette.error.dark, },
  info: { backgroundColor: theme.palette.primary.dark, },
  warning: { backgroundColor: amber[700], },
  icon: { fontSize: 20, },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
})

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props
  const Icon = variantIcon[variant]

  return (
    <SnackbarContent
      className={classNames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  )
}

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
}

const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent)

const override = {
  root: {
    maxWidth: 320,
    bottom: 128
  }
}

class CustomizedSnackbars extends React.Component {
  state = { open: true }

  handleClick = () => { this.setState({ open: true }) }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') return

    this.setState({ open: false })
    console.log('closin')
  }

  render() {
    const { classes, message, variant } = this.props

    return (
      <Snackbar
        className={classes.root}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={this.state.open}
        autoHideDuration={1000}
        onClose={this.handleClose}
        style={{bottom: 96}}
      >
        <MySnackbarContentWrapper
          onClose={this.handleClose}
          variant={ variant }
          message={ message }
        />
      </Snackbar>
    );
  }
}

CustomizedSnackbars.propTypes = {
  message: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(override)(CustomizedSnackbars)


/*

<MySnackbarContentWrapper
  variant="error"
  className={classes.margin}
  message="This is an error message!"
/>
<MySnackbarContentWrapper
  variant="warning"
  className={classes.margin}
  message="This is a warning message!"
/>
<MySnackbarContentWrapper
  variant="info"
  className={classes.margin}
  message="This is an information message!"
/>
<MySnackbarContentWrapper
  variant="success"
  className={classes.margin}
  message="This is a success message!"
/>

*/
