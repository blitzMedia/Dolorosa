import React from 'react'
import { connect } from 'react-redux'
import Navbar from './partials/Navbar'

// Logic evaluation for admin Nabar
const Nav = (props) => {
  const { auth } = props
  if(!auth.uid) return null
  else return <Navbar />
}

const mapStateToProps = (state) => {
  return { auth: state.firebase.auth, }
}

export default connect(mapStateToProps)(Nav)
