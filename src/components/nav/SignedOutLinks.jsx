import React from 'react'
import LockOpenIcon from '@material-ui/icons/LockOpen'
import Button from '@material-ui/core/Button'
import { NavLink } from 'react-router-dom'

const SignedOutLinks = (props) => {
  return (
    <Button
      size="small"
      color="inherit"
      component={NavLink}
      to="/login">
      <LockOpenIcon style={{ fontSize: 20, marginRight: 5 }} />
      Login
    </Button>
  )
}

export default SignedOutLinks
