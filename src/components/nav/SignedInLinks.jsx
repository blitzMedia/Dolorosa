import React from 'react'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import FaceIcon from '@material-ui/icons/Face'
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew'
import Button from '@material-ui/core/Button'
import { NavLink } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { signOut } from '../../store/actions/authActions'

const styles = theme => ({
  iconSmall: {
    fontSize: 20,
    marginRight: 5
  },
})

const SignedInLinks = (props) => {
  const { classes, signOut, userType } = props
  return (
    <>
      {
        userType === 'admin' && (
          <>
            <Button
              size="small"
              color="inherit"
              component={NavLink}
              to="/nuevoUsuario">
              <FaceIcon className={classes.iconSmall} />
              Nuevo Usuario
            </Button>

            <Button
              size="small"
              color="inherit"
              component={NavLink}
              to="/nueva-cita">
              <AddCircleIcon className={classes.iconSmall} />
              Nueva Cita
            </Button>
          </>
        )
      }

      {
        userType === 'cliente' && (
          <Button
            size="small"
            color="inherit"
            component={NavLink}
            to="/nueva-cita">
            <AddCircleIcon className={classes.iconSmall} />
            Nueva Cita
          </Button>
        )
      }

      <Button
        size="small"
        color="inherit"
        onClick={signOut}>
        <PowerSettingsNew className={classes.iconSmall} />
        Logout
      </Button>
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    userType: state.firebase.profile.type
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}

export default compose(
  connect(mapStateToProps,mapDispatchToProps),
  withStyles(styles)
)(SignedInLinks)
