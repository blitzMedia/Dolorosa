import React from 'react'
import { NavLink } from 'react-router-dom'

import MenuItem from '@material-ui/core/MenuItem'
import moment from 'moment'



const Notifications = ({nots}) =>{
  if(!nots) return null
  return (
    nots && nots.map(({cliente, inicio, id = ''}) => (
      <MenuItem key={id} component={NavLink} to={`/${id}`}>
        <strong style={{ marginRight: 4 }}>Nueva cita:</strong>
        { cliente } | {moment(inicio).format('LLLL')}
      </MenuItem>
    ))
  )
}

export default Notifications
