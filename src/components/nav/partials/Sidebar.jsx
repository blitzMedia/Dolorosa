import React from 'react'
import { Link } from 'react-router-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'

import classNames from 'classnames'
//import { makeStyles, useTheme } from '@material-ui/styles'
import { makeStyles } from '@material-ui/styles'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import CalendarIcon from '@material-ui/icons/CalendarToday'
//import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import Divider from '@material-ui/core/Divider'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import MailIcon from '@material-ui/icons/Mail'
import PermIdentity from '@material-ui/icons/PermIdentity'
import Face from '@material-ui/icons/Face'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew'

import { signOut } from '../../../store/actions/authActions'

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: 'width 300ms ease, margin 300ms ease',
  },
  appBarShift: {
    marginLeft: theme.elements.drawerWidth,
    width: `calc(100% - ${theme.elements.drawerWidth}px)`,
    transition: 'width 300ms ease, margin 300ms ease',
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: theme.elements.drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: theme.elements.drawerWidth,
    transition: 'width 300ms ease',
  },
  drawerClose: {
    transition: 'width 300ms ease, margin 300ms ease',
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    '@media (min-width: 600px)': {
      width: theme.spacing.unit * 7.5,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    minHeight: theme.spacing.unit * 6,
    '@media (min-width: 600px)': {
      minHeight: theme.spacing.unit * 8,
    },
    //...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
}))


const Sidebar = (props) => {
/*   state = {
    open: false,
    lists: {
      users: false
    }
  } */

  const classes = useStyles()
  //const theme = useTheme()
  const [open, setOpen] = React.useState(false)
  const [listOpen, setListOpen] = React.useState(false)

  const handleDrawerOpen = () => setOpen(true)
  const handleDrawerClose = () => setOpen(false)
  //const toggleDrawerOpen = () => open ? setOpen(false) : setOpen(true)
  //const handleUsersOpen = () => setListOpen(true)
  const handleUsersClose = () => setListOpen(false)
  const toggleUsersOpen = () => listOpen ? setListOpen(false) : setListOpen(true)

 /*  handleListOpen = what => {
    let newState = this.state
        newState.lists[what] = !this.state.lists[what]
    this.setState({...newState})
  } */
    return (
      <>
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar disableGutters={!open}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={handleDrawerOpen}
              className={classNames(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <MenuIcon />
            </IconButton>
            <aside className="profile">
              Indolora
            </aside>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          anchor="left"
          className={classNames(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: classNames({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
          open={open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={() => { handleDrawerClose(); handleUsersClose() }}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List component="nav" >
            <ListItem button onClick={() => { toggleUsersOpen(); handleDrawerOpen() }}>
              <ListItemIcon>
                <Face />
              </ListItemIcon>
              <ListItemText inset primary="Usuarios" />
              {listOpen ? <ExpandLess /> : <ExpandMore />}
            </ListItem>

            <Collapse in={listOpen} timeout="auto" unmountOnExit id="users">
              <List component="div" disablePadding >
                <ListItem button component={Link} to="/admin/invitar-especialista">
                  <ListItemIcon><PermIdentity /></ListItemIcon>
                  <ListItemText primary='Invitar especialista' />
                </ListItem>
              </List>
            </Collapse>
          </List>

          <Divider />

          <List>
            <ListItem button component={Link} to="/admin">
              <ListItemIcon><CalendarIcon /></ListItemIcon>
              <ListItemText primary="Calendario" />
            </ListItem>
            {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
              <ListItem button key={text}>
                <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>
          <List>
            {['All mail', 'Trash', 'Spam'].map((text, index) => (
              <ListItem button key={text}>
                <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            ))}
          </List>

          <div className="getItDown">
            <ListItem button component="a" href="/" >
              <ListItemIcon>
                <PowerSettingsNew />
              </ListItemIcon>
              <ListItemText inset primary="Logout" />
            </ListItem>
          </div>
        </Drawer>
      </>
    )
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    role: state.auth.role
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut())
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(Sidebar)
