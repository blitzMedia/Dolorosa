import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'

import Notifications from './Notifications'

import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

//import AddIcon from '@material-ui/icons/Add'
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew'
import AccountCircle from '@material-ui/icons/AccountCircle'
import MoreHoriz from '@material-ui/icons/MoreHoriz'
import NotificationsIcon from '@material-ui/icons/Notifications'

//import SignedInLinks from './SignedInLinks'
//import SignedOutLinks from './SignedOutLinks'

const styles = theme => ({
  text: {
    paddingTop: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
  },
  paper: {
    paddingBottom: 50,
  },
  list: {
    marginBottom: theme.spacing.unit * 2,
  },
  subHeader: {
    backgroundColor: theme.palette.background.paper,
  },
  appBar: {
    position: 'static',
    top: 0,
    bottom: 'auto',
    background: 'transparent',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    background: 'white'
  },
  fabButton: {
    position: 'absolute',
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: '0 auto',
  },
  iconSmall: {
    fontSize: 20,
    marginRight: 5
  },
})

class Navbar extends React.Component {
  state = {
    anchorEl: null
  }
  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  }

  render() {
    const { classes, auth, profile, role } = this.props
    const { anchorEl } = this.state

    if( !auth.uid ) return null
    return (
      <AppBar position="fixed" color="inherit" className={classes.appBar} style={{ marginBottom: '1em' }}>
        <Toolbar className={classes.toolbar}>
          <aside className="profile">
              Indolora
              <span> - {profile.displayName}</span>
              <span style={{textTransform: 'capitalize'}}> - {role}</span>
          </aside>

          <div>
            {
              role === 'admin' && (
                <>
                  <IconButton
                    color="inherit"
                    aria-owns={anchorEl ? 'simple-menu' : undefined}
                    aria-haspopup="true"
                    onClick={this.handleClick}
                  >
                    <MoreHoriz />
                  </IconButton>

                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                    >
                    <MenuItem component={NavLink} to="/admin/invitar-especialista">Invitar especialista</MenuItem>
                  </Menu>
                </>
              )
            }
            {
              role !== 'admin' && (
                <IconButton
                  component={NavLink}
                  to="/profile"
                  color="inherit">
                  <AccountCircle />
                </IconButton>
              )
            }

            <>
              <IconButton
                color="inherit"
                aria-owns={anchorEl ? 'simple-menu' : undefined}
                aria-haspopup="true"
                onClick={this.handleClick}
              >
                <NotificationsIcon />
              </IconButton>

              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleClose}
                >
                <Notifications nots={this.props.nots} />
              </Menu>
            </>

            <IconButton
              href="/"
              color="inherit">
              <PowerSettingsNew />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
    )
    //else if (role === 'admin') return <h1>HOLA admin</h1>
  }
}

Navbar.propTypes = { classes: PropTypes.object.isRequired, }

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    role: state.auth.role,
    nots: state.firestore.ordered.notificaciones
  }
}


export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'notificaciones', limit: 3 },
  ]),
  withStyles(styles)
)(Navbar)
