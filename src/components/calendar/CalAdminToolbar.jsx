import React from 'react'

import Typography from '@material-ui/core/Typography'
import Select from '@material-ui/core/Select'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Checkbox from '@material-ui/core/Checkbox'
import ListItemText from '@material-ui/core/ListItemText'

class CalToolbar extends React.Component {
  onChange = (otherTwo) => {
    console.log(otherTwo)
    // this gives us the other two options
    let newOps = this.props.opciones.map(op => {
      const isThisOne = !otherTwo.includes(op.name)
      if(isThisOne) op.selected = !op.selected


      return op
    })
    this.props.onChange(newOps)
  }
  render() {
    const { title, opciones } = this.props
    const opcionList = opciones.map(op => op.name)
    return (
      <div className='calToolbar' style={{ marginBottom: 20 }}>
        <div>
          <Typography variant="h5">
            {title}
          </Typography>
        </div>

        <FormControl>
          <InputLabel htmlFor="select-multiple-checkbox">Display</InputLabel>
          <Select
            multiple
            value={opcionList}
            onChange={ e => this.onChange(e.target.value) }
            input={<Input id="select-multiple-checkbox" />}
            renderValue={selected => selected.join(', ')}
          >
            {opciones.map(({name, selected}, i) => (
              <MenuItem key={name} value={name}>
                <Checkbox checked={selected} />
                <ListItemText primary={name} />
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    )
  }

  navigate = action => {
    console.log(action);

    this.props.onNavigate(action)
  }
}

export default CalToolbar
