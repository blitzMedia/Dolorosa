import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'

import moment from 'moment'

import {   } from '../../store/actions/calActions'
import { checkCitasCal, fetchAllDisponibilidad, fetchMyDisponibilidad, fetchCal, fetchAllCals, } from '../../store/actions/calActions'
import { convertCitasToCalendar } from '../../store/actions/citasActions'
import { newEvent, updateEvent, formatEventsForCalendar, checkForOverlaps } from '../../store/actions/eventActions'

// Toolbars
import CalToolbar from './CalToolbar'
import CalAdminToolbar from './CalAdminToolbar'

// Modales
import ModalCitaClienteNueva from './partials/ModalCitaCliente-Nueva'
import ModalCitaClienteUpdate from './partials/ModalCitaCliente-Update'
import ModalCitaAceptar from './partials/ModalCita-Aceptar'
import ModalCitaAdminUpdate from './partials/ModalCitaAdmin-Update'
import ModalEvento from '../layout/especialistas/ModalEvento'

import Dialog from '@material-ui/core/Dialog'
import BigCalendar from 'react-big-calendar'
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop'
import 'react-big-calendar/lib/addons/dragAndDrop/styles.css'

import Card from '../../UI/material-dashboard-pro-react/src/components/Card/Card.jsx'
import CardBody from '../../UI/material-dashboard-pro-react/src/components/Card/CardBody.jsx'// Core components
import GridContainer from '../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx'
import GridItem from '../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx'

const localizer = BigCalendar.momentLocalizer(moment)
const DragAndDropCalendar = withDragAndDrop(BigCalendar)


class SuperCal extends Component {
  // Component basics
  constructor(props) {
    super(props)
    this.state = {
      opciones: [
        {
          name: "Disponibilidad",
          selected: true
        },
        {
          name: "Citas",
          selected: true
        },
      ],
      UI: {
        alert: null,
        modal: {
          content: null,
          open: false
        }
      }
    }
    this.cal = React.createRef()
  }

  componentWillMount() {
    this.props.fetchAllDisponibilidad()
    this.props.checkCitasCal()
  }

  componentDidUpdate(prevProps) {
    console.log(4, 'componentDidUpdate', this.props.match.params)
    if (this.props.citas !== prevProps.citas && this.props.match.params.id) {
      const {id} = this.props.match.params
      const theCita = this.props.citas.find(cita => cita.id === id)

      this.handleModalOpen(theCita)
    }
  }

  // Cosas

  handleSlot = async (slot) => {
    const isMonthView = document.querySelector('.rbc-month-view')

    if( this.props.role === 'especialista' && !isMonthView ) {
      this.props.newEvent(slot)
    }
  }

  // Methods
  handleModalOpen = (event) => {
    document.documentElement.classList.add('noscroll')

    event.type = event.type || ''
    const isDisp = event.type.includes('disponibilidad') || event.type.includes('client')
    const { role } = this.props

    let modal = null, open = true

    // Oh my
    // Si es disponibilidad
    if (isDisp) {
      // Si le da un cliente a un hueco de disponibilidad, es que quiere una nueva cita
      if (role === 'cliente') modal = <ModalCitaClienteNueva slot={event} onClose={this.handleModalClose} />
      // Si es admin o especialista, entonces quiere cambiarla
      else modal = <ModalEvento event={event} onClose={this.handleModalClose} />

    // Y ya si es cita
    } else {
      if (role === 'admin') modal = <ModalCitaAdminUpdate slot={event} onClose={this.handleModalClose} />
      else if (role === 'especialista') modal = <ModalCitaAceptar slot={event} onClose={this.handleModalClose} />
      else if (role === 'cliente') {
        if (event.autorizada && event.autorizada.porAdmin) modal = <ModalCitaAceptar slot={event} onClose={this.handleModalClose} />
        else modal = <ModalCitaClienteUpdate slot={event} onClose={this.handleModalClose} />
      }
    }

    this.setState({
      ...this.state,
      modal: {
        content: modal,
        open
      }
    })

  }

  handleModalClose = value => {
    document.documentElement.classList.remove('noscroll')
    this.setState({
      modal: {
        content: null,
        open: false
      }
    })
  }

  // Events
  eventType = (event, start, end, isSelected) => {
    let type = 'event-'

    // Si es disponibilidad
    if (event.type.includes('disp') || event.type.includes('client')) type = type + 'disponibilidad'

    else if (event.type.includes('cit')) {
      type = type + 'cita'
      if ( event.autorizada.porAdmin ) type = type + ' cita-admin-ok'
      if ( event.autorizada.porCliente ) type = type + ' cita-cliente-ok'
    }
    else type = type + 'default'

    return { className: type }
  }

  detectDuplicates = (citas) => citas.filter(cita => cita.id !== "")
  filterLockedDisps = disps => disps.filter(disp => !disp.summary.includes('lock'))

  handleSelectChange = (opciones) => {
    this.setState({opciones})
  }

  render() {
    console.log(2, 'render',  'SUPER CAL PROPS', this.props)

    // Modales
    const modal = this.state.modal && (
      <Dialog
        className={this.props.role !== 'admin' && "modalConDatepicker"}
        open={this.state.modal.open}
        onClose={this.handleModalClose}
        fullWidth
        scroll="body"
      >
        {this.state.modal.content}
        <span></span>
      </Dialog>
    )


    const { role, auth } = this.props


    // Important dates
    const max = new Date(), min = new Date()
    max.setHours(21, 0, 0); min.setHours(7, 0, 0)

    // Calculamos quién ve qué
    let { disponibilidades = [], citas = [] } = this.props
    citas = convertCitasToCalendar(citas)

    // Cálculo de eventos!
    let events, dispsArray, citasArray
    if (role === 'admin') {
      // We evaluate if option is selected
      const isDispSelected = this.state.opciones.find(op => op.name === 'Disponibilidad').selected,
            isCitasSelected = this.state.opciones.find(op => op.name === 'Citas').selected

      // We use the eval to return the full array or just an empty carcass
      dispsArray =  isDispSelected ? formatEventsForCalendar(disponibilidades, 'disponibilidad') : []
      citasArray =  isCitasSelected ? citas : []
    } else if (role === 'especialista') {
      dispsArray = formatEventsForCalendar(disponibilidades, 'disponibilidad')
      citasArray = citas
    } else if (role === 'cliente') {
      dispsArray = checkForOverlaps( this.filterLockedDisps(disponibilidades) )
      citasArray = this.detectDuplicates(citas)
    }
    // Finally we concat it all
    events = [...dispsArray, ...citasArray]


    return (
      <>
        { auth.isEmpty && (<Redirect to="/" />) }

        {this.state.alert}

        {modal}


        <GridContainer className="General">
          <GridItem xs={12} sm={12} md={10}>
            {
              events ? (
                <>
                  { role === 'admin' && (
                      <CalAdminToolbar
                        onChange={this.handleSelectChange}
                        title="Calendario completo"
                        opciones={this.state.opciones}
                      />
                    )
                  }
                  <Card>
                    <CardBody>

                      {
                        (role === 'admin' || role === 'especialista') &&
                          <DragAndDropCalendar
                            ref={this.cal}
                            selectable
                            resizable
                            onEventResize={event => this.props.updateEvent(event)}
                            onEventDrop={event => this.props.updateEvent(event)}
                            localizer={localizer}
                            events={events}
                            views={['day', 'agenda', 'work_week', 'month']}
                            defaultView='work_week'
                            scrollToTime={new Date(1970, 1, 1, 6)}
                            defaultDate={new Date()}
                            max={ max }
                            min={ min }
                            components = {{toolbar : CalToolbar}}
                            onSelectSlot={(slot) => this.handleSlot(slot)}
                            onSelectEvent={event => this.handleModalOpen(event)}
                            eventPropGetter={this.eventType}
                            className="calMulti"
                          />
                      }

                      {
                        role === 'cliente' &&
                        <BigCalendar
                          localizer={localizer}
                          events={events}
                          views={['day', 'agenda', 'work_week', 'month']}
                          defaultView='work_week'
                          scrollToTime={new Date(1970, 1, 1, 6)}
                          defaultDate={new Date()}
                          max={ max }
                          min={ min }
                          components = {{toolbar : CalToolbar}}
                          onSelectSlot={(slot) => this.handleSlot(slot)}
                          onSelectEvent={event => this.handleModalOpen(event)}
                          eventPropGetter={this.eventType}
                          className="calMulti clientes"
                        />
                      }

                    </CardBody>
                  </Card>
                </>
              ) : null
            }

          </GridItem>
        </GridContainer>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  //console.clear()
  console.log(1, 'mapStateToProps', 'SUPER CAL STATE', state)
  return {
    especialistas: state.firestore.ordered.users,
    citas: state.firestore.ordered.citas,
    disponibilidades: state.cal.disponibilidades,
    role: state.auth.role,
    auth: state.firebase.auth,
    profile: state.firebase.profile
    //authApp: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCal: (calId) => dispatch(fetchCal(calId)),
    fetchAllCals: () => dispatch(fetchAllCals()),
    checkCitasCal: () => dispatch(checkCitasCal()),
    fetchAllDisponibilidad: () => dispatch(fetchAllDisponibilidad()),
    fetchMyDisponibilidad: () => dispatch(fetchMyDisponibilidad()),
    newEvent: (slot) => dispatch(newEvent(slot)),
    updateEvent: (slot) => dispatch(updateEvent(slot)),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect([
    { collection: 'citas' },
    { collection: 'users', where: [['role', '==', 'especialista']] },
  ])
)(SuperCal)
