import React from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'

import {
  cita_ClienteToAdminDB
} from '../../../store/actions/citasActions'

import { withStyles } from '@material-ui/core/styles'
//import { formatEventsForCalendar, listEventsById } from '../../../../calendar/calendarHelpers'

import Button from '@material-ui/core/Button'
//import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import FormLabel from "@material-ui/core/FormLabel"

import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import Check from "@material-ui/icons/Check"
import Today from "@material-ui/icons/Today"
import Slider from '@material-ui/lab/Slider'

import CustomInput from "../../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx"
import GridContainer from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx"
import GridItem from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx"
import CardHeader from "../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"
import CardText from "../../../UI/material-dashboard-pro-react/src/components/Card/CardText.jsx"

import Datetime from "react-datetime"
import moment from "moment"

const styles = {
  dialogFooter: {
    justifyContent: 'space-between'
  },
}

class ModalCita extends React.Component {
  state = {
    slot: {
      start: '',
      end: ''
    },
    cita: {
      checkedEspeciales: false,
      duracion: 60
    },
    initRequired: true
  }

  componentWillMount() {
    const { slot: { start, end }, nueva } = this.props
    this.setState({
      cita: { ...this.state.cita, start: moment(start).format('MM/DD/YYYY HH:mm:ss') },
      slot: {start: moment(start), end: moment(end)}
    })

    if(!nueva) {
      console.warn(this.props)
      const { cita, cita: { start, end, duracion }, id } = this.props.slot
      this.setState({
        cita: {
          ...cita,
          duracion,
          start: moment(start).format('MM/DD/YYYY HH:mm:ss'),
          end: moment(end).format('MM/DD/YYYY HH:mm:ss'),
          id: id
        }
      })
    }
  }

  onSubmit = (e) => {
    e.preventDefault()

    const action = this.props.slot.type === 'disponibilidad' ? 'create' : 'update'
    this.props.cita_ClienteToAdminDB(this.state.cita, action, {id: this.state.cita.id})
    this.props.onClose()
  }

  handleDelete = (e) => {
    e.preventDefault()
    this.props.cita_ClienteToAdminDB(this.state.cita, 'delete', {id: this.state.cita.id})
    this.props.onClose()
  }

  handleChange = (e) => {
    this.setState({cita: {
      ...this.state.cita,
      [e.target.id]: e.target.value
    }})
  }

  onChangeSlot = (slot) => {
    const { slot: { end }, cita } = this.state
    this.setState({
      slot: { start: slot, end },
      cita: { ...cita, start: moment(slot).toISOString() }
    })
  }

  handleCheck = e => {
    this.setState({ cita: {
      ...this.state.cita,
      checkedEspeciales: e.target.checked
    }})
  }

  handleSlider = (event, value) => {
    this.setState({cita: { ...this.state.cita, duracion: value }});
  }

  validate = (e) => {
    this.props.cita_ClienteToAdminDB(this.state.cita, 'update', {id: this.state.cita.id, auther: !this.props.autorizada })
    this.props.onClose()
  }

  render() {
    const { nueva, role, classes } = this.props
    const admin = role === 'especialista' || role === 'admin'

    if(!nueva) {
      const { cliente, cita: { checkedEspeciales, nombre_paciente, procedimiento, start, tel_paciente, observaciones }, autorizada, end  } = this.props.slot
      const { duracion } = this.state.cita

      // Time constraints
      const startDate = moment(start), endDate = moment(end)
      const minMaxDatetime = {
        minutes: { step: 30 },
        hours: { min: startDate.hours(), max: endDate.hours(), step: 1 }
      }
      const modalTitle = admin ? cliente : nombre_paciente

      return (
        <form onSubmit={this.onSubmit}>
          <CardHeader color="rose" icon>
            <CardText color="rose" className="iconHeader  greenHeader">
              <Today />
              <h4>{`${modalTitle} - ${startDate.format('LL')}`}</h4>
            </CardText>
          </CardHeader>
          <DialogContent style={{ overflow: 'visible', marginTop: 20 }}>

            <Datetime
              id="date-start"
              defaultValue={startDate}
              dateFormat={false}
              inputProps={{
                'placeholder': 'Inicio',
                'required': this.state.initRequired
              }}
              onChange={slot => this.onChangeSlot(slot)}
              timeConstraints={minMaxDatetime}
            />

            <CustomInput
              labelText="Nombre del paciente"
              id="nombre_paciente"
              formControlProps={{fullWidth: true}}
              inputProps={{
                onChange: this.handleChange,
                required: this.state.initRequired,
                defaultValue: nombre_paciente
              }}
            />
            <CustomInput
              labelText="Teléfono del paciente"
              id="tel_paciente"
              formControlProps={{fullWidth: true}}
              inputProps={{
                type: "number",
                onChange: this.handleChange,
                required: this.state.initRequired,
                defaultValue: tel_paciente
              }}
            />
            <CustomInput
              labelText="Procedimiento"
              id="procedimiento"
              formControlProps={{fullWidth: true}}
              inputProps={{
                onChange: this.handleChange,
                required: this.state.initRequired,
                defaultValue: procedimiento
              }}
            />

            <GridContainer style={{
              marginTop: 30,
              marginBottom: 10,
              display: 'flex',
              alignItems: 'center'
            }}>
            <GridItem xs={12} sm={4}>
              <FormLabel className="labelSlider">
                Duración estimada: { this.state.cita.duracion }'
              </FormLabel>
            </GridItem>
            <GridItem xs={12} sm={8}>
              <Slider
                id="procedimiento_duracion"
                style={{ padding: '10px 0px' }}
                value={duracion}
                min={0}
                max={150}
                step={30}
                onChange={this.handleSlider}
              />
            </GridItem>
          </GridContainer>

          <CustomInput
            labelText="Observaciones"
            id="observaciones"
            formControlProps={{fullWidth: true}}
            inputProps={{
              multiline: true,
              onChange: this.handleChange,
              defaultValue: observaciones || ''
            }}
          />

          <FormControlLabel
            control={
              <Checkbox
                tabIndex={-1}
                checked={checkedEspeciales}
                onClick={e => this.handleCheck(e)}
                checkedIcon={<Check style={{
                  width: "20px",
                  height: "20px",
                  border: "1px solid rgba(0, 0, 0, .54)",
                  borderRadius: "3px",
                  color: "black"
                }} />}
                icon={<Check style={{
                  width: "0px",
                  height: "0px",
                  padding: "9px",
                  border: "1px solid rgba(0, 0, 0, .54)",
                  borderRadius: "3px"
                }} />}
              />
            }
            label="Niño o necesidades especiales"
          />
          </DialogContent>

          <DialogActions className={classes.dialogFooter}>
            <div>
              <Button color="secondary" onClick={this.props.onClose}>
                { admin ? <span>Salir</span> : <span>Cancelar</span> }
              </Button>
              <Button color="primary" type="submit">
                { admin ? <span>Modificar</span> : <span>Guardar</span> }
              </Button>
              <Button color="primary" onClick={this.handleDelete}>
                Eliminar
              </Button>
            </div>
            <div>
              <Button variant="outlined" color="primary" onClick={this.validate}>
                { autorizada ? <span>Anular</span> : <span>Validar</span> }
              </Button>
            </div>
          </DialogActions>
        </form>
      )
    }

    else if (nueva) {
      const { slot: { start, end } } = this.props

      // Time constraints
      const startDate = moment(start), endDate = moment(end)
      const minMaxDatetime = {
        minutes: { step: 30 },
        hours: { min: startDate.hours(), max: endDate.hours(), step: 1 }
      }

      return (
        <form onSubmit={this.onSubmit}>
          <CardHeader color="rose" icon>
            <CardText color="rose" className="iconHeader greenHeader">
              <Today />
              <h4>Nueva cita - {startDate.format('LL')}</h4>
            </CardText>
          </CardHeader>
          <DialogContent style={{ overflow: 'visible', marginTop: 20 }}>
            <Datetime
              id="date-start"
              defaultValue={this.state.slot.start}
              dateFormat={false}
              inputProps={{
                'placeholder': 'Inicio',
                'required': this.state.initRequired
              }}
              onChange={slot => this.onChangeSlot(slot)}
              timeConstraints={minMaxDatetime}
              isValidDate={this.isValid}
            />

            <CustomInput
              labelText="Nombre del paciente"
              id="nombre_paciente"
              formControlProps={{fullWidth: true}}
              inputProps={{
                onChange: this.handleChange,
                required: this.state.initRequired
              }}
            />
            <CustomInput
              labelText="Teléfono del paciente"
              id="tel_paciente"
              formControlProps={{fullWidth: true}}
              inputProps={{
                type: "number",
                onChange: this.handleChange,
                required: this.state.initRequired
              }}
            />
            <CustomInput
              labelText="Procedimiento"
              id="procedimiento"
              formControlProps={{fullWidth: true}}
              inputProps={{
                onChange: this.handleChange,
                required: this.state.initRequired
              }}
            />

            <GridContainer style={{
              marginTop: 30,
              marginBottom: 10,
              display: 'flex',
              alignItems: 'center'
            }}>
            <GridItem xs={12} sm={4}>
              <FormLabel className="labelSlider">
                Duración {admin || (<span> estimada</span>)}: { this.state.cita.duracion }'
              </FormLabel>
            </GridItem>
            <GridItem xs={12} sm={8}>
              <Slider
                id="procedimiento_duracion"
                style={{ padding: '10px 0px' }}
                value={this.state.cita.duracion}
                min={0}
                max={150}
                step={30}
                onChange={this.handleSlider}
              />
            </GridItem>
          </GridContainer>

          <CustomInput
            labelText="Observaciones"
            id="observaciones"
            formControlProps={{fullWidth: true}}
            inputProps={{
              multiline: true,
              onChange: this.handleChange
            }}
          />
          <FormControlLabel
            control={
              <Checkbox
                tabIndex={-1}
                checked={this.state.checkedEspeciales}
                onClick={e => this.handleCheck(e)}
                checkedIcon={<Check style={{
                  width: "20px",
                  height: "20px",
                  border: "1px solid rgba(0, 0, 0, .54)",
                  borderRadius: "3px",
                  color: "black"
                }} />}
                icon={<Check style={{
                  width: "0px",
                  height: "0px",
                  padding: "9px",
                  border: "1px solid rgba(0, 0, 0, .54)",
                  borderRadius: "3px"
                }} />}
              />
            }
            label="Niño o necesidades especiales"
          />
          </DialogContent>

          <DialogActions>
            <Button color="primary" onClick={this.props.onClose}>
              Cancelar
            </Button>
            <Button color="primary" type="submit">
              Guardar
            </Button>
          </DialogActions>
        </form>
      )
    }

    else return null
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    cita_ClienteToAdminDB: (cita, action, auther) => dispatch(cita_ClienteToAdminDB(cita, action, auther))
  }
}

const mapStateToProps = (state) => {
  console.warn('STATE', state)
  return {
    role: state.firebase.profile.role
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(ModalCita)
