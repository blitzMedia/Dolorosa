import React from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'

import { citaToAdminDB, aceptarCita } from '../../../store/actions/citasActions'

import { withStyles } from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
//import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Today from "@material-ui/icons/Today"

import CardHeader from "../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"
import CardText from "../../../UI/material-dashboard-pro-react/src/components/Card/CardText.jsx"

import moment from "moment"

const styles = {
  dialogFooter: {
    justifyContent: 'space-between'
  },
}

class ModalCitaEspecialista extends React.Component {
  state = {}

  calcEnd = (start, duracion) => {
    return moment(start).clone().add(duracion, 'minutes').format('MM/DD/YYYY HH:mm:ss')
  }

  validate = (e) => {
    this.props.citaToAdminDB(this.state.cita, 'validate')
    this.props.onClose()
  }

  aceptar = async id => {
    await this.props.aceptarCita(id)
    this.props.onClose()
  }

  render() {
    // Rapidamente de los props
    const { classes } = this.props

    // Del slot
    let startDate = moment(this.props.slot.start), endDate = moment(this.props.slot.end)

    let {
      id,
      cliente: {
        displayName
      },
      detalles: {
        tel_paciente,
        procedimiento,
        observaciones,
        checkedEspeciales,
      }
    } = this.props.slot

    return (
      <form onSubmit={this.onSubmit}>
        <CardHeader color="rose" icon>
          <CardText color="rose" className="iconHeader greenHeader">
            <Today />
            <h4>{`${startDate.format('LL')}`}</h4>
          </CardText>
        </CardHeader>

        <DialogContent style={{ overflow: 'visible', marginTop: 30 }}>

          <p><strong>Hora:</strong> {startDate.format("HH:mm")} - {endDate.format("HH:mm")}</p>
          <p><strong>Cliente:</strong> {displayName}</p>
          <p><strong>Teléfono:</strong> {tel_paciente}</p>
          <p><strong>Procedimiento:</strong> {procedimiento}</p>
          <p><strong>Observaciones:</strong> {observaciones}</p>
          <p><strong>Niño o necesidades especiales:</strong> {checkedEspeciales ? 'Sí' : 'No'}</p>
        </DialogContent>

        <DialogActions className={classes.dialogFooter}>
          <Button color="secondary" onClick={this.props.onClose}>
            Cancelar
          </Button>
          <Button color="primary" onClick={() => this.aceptar(id)}>
            Aceptar cita
          </Button>
        </DialogActions>
      </form>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    citaToAdminDB: (cita, action) => dispatch(citaToAdminDB(cita, action)),
    aceptarCita: (id) => dispatch(aceptarCita(id))
  }
}

const mapStateToProps = (state) => {
  return {
    role: state.firebase.profile.role,
    profile: state.firebase.profile
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(ModalCitaEspecialista)
