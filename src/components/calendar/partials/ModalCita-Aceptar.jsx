import React from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'

import { citaToAdminDB, aceptarCita } from '../../../store/actions/citasActions'
import { bloquearDisponibilidad } from '../../../store/actions/eventActions'

import { withStyles } from '@material-ui/core/styles'

import Button from '@material-ui/core/Button'
//import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Today from "@material-ui/icons/Today"

import CardHeader from "../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"
import CardText from "../../../UI/material-dashboard-pro-react/src/components/Card/CardText.jsx"

import moment from "moment"

const styles = {
  dialogFooter: {
    justifyContent: 'space-between'
  },
}

class ModalCitaAceptar extends React.Component {
  state = {}

  calcEnd = (start, duracion) => {
    return moment(start).clone().add(duracion, 'minutes').format('MM/DD/YYYY HH:mm:ss')
  }

  validate = (e) => {
    this.props.citaToAdminDB(this.state.cita, 'validate')
    this.props.onClose()
  }

  aceptar = id => {
    const myDisp = this.props.slot.slotDetails.find(rec => rec.especialista === this.props.profile.email)
    console.log(myDisp)
    this.props.aceptarCita(id, this.props.role)
    this.props.bloquearDisponibilidad(myDisp.eventId)
    this.props.onClose()
  }

  render() {
    // Rapidamente de los props
    const { classes } = this.props
    const autorizadaPorEspi = this.props.slot.slotDetails.find(rec => rec.especialista === this.props.profile.email).ok

    // Del slot
    let startDate = moment(this.props.slot.start), endDate = moment(this.props.slot.end)

    let {
      id,
      cliente: {
        displayName
      },
      detalles: {
        nombre_paciente,
        tel_paciente,
        procedimiento,
        observaciones,
        checkedEspeciales,
      },
      autorizada: { porAdmin: autorizadaPorAdmin }
    } = this.props.slot

    return (
      <form onSubmit={this.onSubmit}>
        <CardHeader color="rose" icon>
          <CardText color="rose" className="iconHeader greenHeader">
            <Today />
            <h4>{`${startDate.format('LL')}`}</h4>
          </CardText>
        </CardHeader>

        <DialogContent style={{ overflow: 'visible', marginTop: 30 }}>

          <p><strong>Hora:</strong> {startDate.format("HH:mm")} - {endDate.format("HH:mm")}</p>
          <p><strong>Cliente:</strong> {displayName}</p>
          <p><strong>Teléfono:</strong> {tel_paciente}</p>
          <p><strong>Procedimiento:</strong> {procedimiento}</p>
          <p><strong>Observaciones:</strong> {observaciones}</p>
          <p><strong>Niño o necesidades especiales:</strong> {checkedEspeciales ? 'Sí' : 'No'}</p>
        </DialogContent>

        <DialogActions className={classes.dialogFooter}>
          <Button color="secondary" variant="contained" onClick={this.props.onClose}>Cancelar</Button>
          { // Si ya está autorizada, el botón está disabled
            autorizadaPorAdmin || autorizadaPorEspi
              ? (<Button color="primary" variant="contained" disabled>Cita ya autorizada</Button>)
              : (<Button color="primary" variant="contained" onClick={() => this.aceptar(id)}>Aceptar cita</Button>)
          }
        </DialogActions>
      </form>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    citaToAdminDB: (cita, action) => dispatch(citaToAdminDB(cita, action)),
    bloquearDisponibilidad: (id) => dispatch(bloquearDisponibilidad(id)),
    aceptarCita: (id, role) => dispatch(aceptarCita(id, role))
  }
}

const mapStateToProps = (state) => {
  return {
    role: state.auth.role,
    profile: state.firebase.profile
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(ModalCitaAceptar)
