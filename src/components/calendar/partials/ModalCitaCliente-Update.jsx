import React from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'

import { citaToAdminDB } from '../../../store/actions/citasActions'

import { withStyles } from '@material-ui/core/styles'
//import { formatEventsForCalendar, listEventsById } from '../../../../calendar/calendarHelpers'

import Button from '@material-ui/core/Button'
//import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import FormLabel from "@material-ui/core/FormLabel"

import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import Check from "@material-ui/icons/Check"
import Today from "@material-ui/icons/Today"
import Slider from '@material-ui/lab/Slider'

import CustomInput from "../../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx"
import GridContainer from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx"
import GridItem from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx"
import CardHeader from "../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"
import CardText from "../../../UI/material-dashboard-pro-react/src/components/Card/CardText.jsx"

import Datetime from "react-datetime"
import moment from "moment"

const styles = {
  dialogFooter: {
    justifyContent: 'space-between'
  },
}

class ModalCitaClienteUpdate extends React.Component {
  state = {
    slot: {
      start: '',
      end: ''
    },
    cita: {
      autorizada: {
        porAdmin: false,
        porCliente: false
      },
      createdAt: '',
      cliente: {},
      id: '',
      detalles: {
        start: '',
        duracion: 60,
        end: '',
        nombre_paciente: '',
        tel_paciente: '',
        procedimiento: '',
        observaciones: '',
        checkedEspeciales: false,
      }
    },
    cambio: false,
    // Required / Validation
    initRequired: true
  }

  componentWillMount() {

    console.warn('THIS ARE PROPS', this.props)

    const {
      start, end,
      autorizada: {
        porAdmin,
        porCliente,
      },
      cliente,
      id,
      detalles,

    } = this.props.slot

    // Seteamos estado inicial
    this.setState({
      ...this.state,
      slot: {start, end},
      cita: {
        ...this.state.cita,
        autorizada: {
          porAdmin,
          porCliente
        },
        cliente,
        id,
        detalles: {
          ...detalles,
        }
      }
    })
  }

  calcEnd = (start, duracion) => {
    return moment(start).clone().add(duracion, 'minutes').format('MM/DD/YYYY HH:mm:ss')
  }

  onSubmit = (e) => {
    e.preventDefault()
    this.props.citaToAdminDB(this.state.cita, 'update')
    this.props.onClose()
  }

  handleDelete = (e) => {
    e.preventDefault()
    this.props.citaToAdminDB(this.state.cita, 'delete')
    this.props.onClose()
  }

  handleChange = (e) => {
    this.setState({
      ...this.state,
      cita: {
        ...this.state.cita,
        detalles: {
          ...this.state.cita.detalles,
          [e.target.id]: e.target.value
        }
      },
      cambio: true
    })
  }

  onChangeStart = (slot) => {
    this.setState({
      ...this.state,
      slot: {
        ...this.state.slot,
        start: slot
      },
      cita: {
        ...this.state.cita,
        detalles: {
          ...this.state.cita.detalles,
          start: moment(slot).toISOString(),
          end: this.calcEnd(moment(slot), this.state.cita.detalles.duracion)
        }
      },
      cambio: true
    })
  }

  handleCheck = e => {
    this.setState({
      ...this.state,
      cita: {
        ...this.state.cita,
        detalles: {
          ...this.state.cita.detalles,
          checkedEspeciales: e.target.checked
        }
      },
      cambio: true
    })
  }

  handleSlider = (event, value) => {
    this.setState({
      ...this.state,
      cita: {
        ...this.state.cita,
        detalles: {
          ...this.state.cita.detalles,
          duracion: value,
          end: this.calcEnd(moment(this.state.cita.detalles.start), value)
        }
      },
      cambio: true
    })
  }
  /*
    validate = (e) => {
      this.props.cita_ClienteToAdminDB(this.state.cita.detalles, 'update', {id: this.state.cita.id, auther: !this.props.autorizada })
      this.props.onClose()
    }
  */
  render() {
    // Rapidamente de los props
    //const { nueva, classes } = this.props
    const { start } = this.props.slot.detalles

    // Del slot
    // Time constraints para el date picker
    let startDate = moment(this.props.slot.start), endDate = moment(this.props.slot.end)
    let minMaxDatetime = {
      minutes: { step: 30 },
      hours: {
        min: startDate.hours(),
        max: endDate.hours(),
        step: 1
      }
    }

    let {
      cita: {
        //cliente: {},
        detalles: {
          duracion,
          nombre_paciente,
          tel_paciente,
          procedimiento,
          observaciones,
          checkedEspeciales,
        }
      }
    } = this.state

    // Calculando la diferencia en minutos desde el principio hasta el final del slot de disponibilidad
    let diffStart = moment(this.state.slot.start),
    diffEnd = moment(this.props.slot.end),
    diff = diffEnd.diff(diffStart, 'minutes')



    return (
      <form onSubmit={this.onSubmit}>
        <CardHeader color="rose" icon>
          <CardText color="rose" className="iconHeader greenHeader">
            <Today />
            <h4>{`${startDate.format('LL')}`}</h4>
          </CardText>
        </CardHeader>

        <DialogContent style={{ overflow: 'visible', marginTop: 20 }}>
          <Datetime
            id="date-start"
            defaultValue={moment(start)}
            dateFormat={false}
            inputProps={{
              'placeholder': 'Inicio',
              'required': this.state.initRequired
            }}
            onChange={slot => this.onChangeStart(slot)}
            timeConstraints={minMaxDatetime}
          />

          <CustomInput
            labelText="Nombre del paciente"
            id="nombre_paciente"
            formControlProps={{fullWidth: true}}
            inputProps={{
              onChange: this.handleChange,
              required: this.state.initRequired,
              defaultValue: nombre_paciente
            }}
          />
          <CustomInput
            labelText="Teléfono del paciente"
            id="tel_paciente"
            formControlProps={{fullWidth: true}}
            inputProps={{
              type: "number",
              onChange: this.handleChange,
              required: this.state.initRequired,
              defaultValue: tel_paciente
            }}
          />
          <CustomInput
            labelText="Procedimiento"
            id="procedimiento"
            formControlProps={{fullWidth: true}}
            inputProps={{
              onChange: this.handleChange,
              required: this.state.initRequired,
              defaultValue: procedimiento
            }}
          />

          <GridContainer style={{
            marginTop: 30,
            marginBottom: 10,
            display: 'flex',
            alignItems: 'center'
          }}>
          <GridItem xs={12} sm={4}>
            <FormLabel className="labelSlider">
              Duración estimada: { duracion }'
            </FormLabel>
          </GridItem>
          <GridItem xs={12} sm={8}>
            <Slider
              id="procedimiento_duracion"
              style={{ padding: '10px 0px' }}
              value={duracion}
              min={30}
              max={diff}
              step={30}
              onChange={this.handleSlider}
            />
          </GridItem>
        </GridContainer>

        <CustomInput
          labelText="Observaciones"
          id="observaciones"
          formControlProps={{fullWidth: true}}
          inputProps={{
            multiline: true,
            onChange: this.handleChange,
            defaultValue: observaciones
          }}
        />

        <FormControlLabel
          control={
            <Checkbox
              tabIndex={-1}
              checked={checkedEspeciales}
              onClick={e => this.handleCheck(e)}
              checkedIcon={<Check style={{
                width: "20px",
                height: "20px",
                border: "1px solid rgba(0, 0, 0, .54)",
                borderRadius: "3px",
                color: "black"
              }} />}
              icon={<Check style={{
                width: "0px",
                height: "0px",
                padding: "9px",
                border: "1px solid rgba(0, 0, 0, .54)",
                borderRadius: "3px"
              }} />}
            />
          }
          label="Niño o necesidades especiales"
        />
        </DialogContent>

        <DialogActions>
          <Button color="primary" onClick={this.handleDelete}>
            Eliminar
          </Button>
          <Button color="secondary" onClick={this.props.onClose}>
            Cancelar
          </Button>
          {
            this.state.cambio && (
              <Button color="primary" type="submit">
                Guardar
              </Button>
            )
          }
        </DialogActions>
      </form>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    citaToAdminDB: (cita, action) => dispatch(citaToAdminDB(cita, action))
  }
}

const mapStateToProps = (state) => {
  console.warn('STATE', state)
  return {
    cal: state.cal,
    role: state.firebase.profile.role,
    profile: state.firebase.profile
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(ModalCitaClienteUpdate)
