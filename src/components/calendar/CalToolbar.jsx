import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'


import IconButton from '@material-ui/core/IconButton'
import RightArrowIcon from '@material-ui/icons/ChevronRight'
import LeftArrowIcon from '@material-ui/icons/ChevronLeft'

class CalToolbar extends Component {
  handleChange = (event) => {
    this.props.onView(event.target.value);
  }

  formatNames = (name) => name.replace('_', ' ')

  render() {
    const { view, views, label } = this.props

    return (
      <div className='calToolbar' style={{ marginBottom: 20, padding: 10 }}>
        <div className="buttonGroup flexCenter horizontal">
          <IconButton onClick={() => this.navigate('PREV')}><LeftArrowIcon /></IconButton>
          <Typography variant="h5" style={{ textTransform: 'capitalize', width: '100%' }}>{label}</Typography>
          <IconButton onClick={() => this.navigate('NEXT')}><RightArrowIcon /></IconButton>
        </div>

        <FormControl style={{ marginLeft: 16, minWidth: 120 }}>
          <Select
            value={view}
            onChange={this.handleChange}
            className="capitalize"
          >
            {views.map((value) => <MenuItem key={value} value={value}>{this.formatNames(value)}</MenuItem>)}
          </Select>
        </FormControl>
      </div>
    )
  }

  navigate = action => {
    console.log(action);
    this.props.onNavigate(action)
  }
}

export default CalToolbar
