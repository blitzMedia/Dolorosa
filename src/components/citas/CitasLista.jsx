import React from 'react'
import CitaResumen from './CitaResumen'

const CitasLista = ({citas}) => {
  return (
    <section className="vertical">
      { citas && citas.map(cita => <CitaResumen key={cita.id} cita={cita} />) }
    </section>
  )
}

export default CitasLista
