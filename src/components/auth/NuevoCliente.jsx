import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { clienteSignUp } from '../../store/actions/authActions'

import FlexContainer from 'react-styled-flexbox'
import CustomizedSnackbars from '../UI/ComplexSnackbars'

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
import InputAdornment from '@material-ui/core/InputAdornment'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import FormControl from '@material-ui/core/FormControl'

// @material-ui/icons
import Email from '@material-ui/icons/Email'
import ContactPhone  from '@material-ui/icons/ContactPhone'
import Lock  from '@material-ui/icons/Lock'
import LockOpen  from '@material-ui/icons/LockOpen'

// core components
import GridContainer from '../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx'
import GridItem from '../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx'
import Button from '../../UI/material-dashboard-pro-react/src/components/CustomButtons/Button.jsx'
import Card from '../../UI/material-dashboard-pro-react/src/components/Card/Card.jsx'
import CardBody from '../../UI/material-dashboard-pro-react/src/components/Card/CardBody.jsx'
import CardHeader from '../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx'
import CardFooter from '../../UI/material-dashboard-pro-react/src/components/Card/CardFooter.jsx'

import loginPageStyle from '../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx'

import MUIPlacesAutocomplete, { geocodeByPlaceID } from 'mui-places-autocomplete'

class NuevoCliente extends Component {
  constructor(props) {
    super(props)

    this.confirmPassword = React.createRef()
    this.state = {
      lugar: {},
      email: '',
      tel: '',
      password: '',
      confirmPassword: ''
    }
  }

  onSuggestionSelected = async (suggestion) => {
    console.log(suggestion)
    let result, lugar = {}
    try {
      result = await geocodeByPlaceID(suggestion.place_id)
      lugar = {
        name: suggestion.structured_formatting.main_text,
        address: result[0].formatted_address,
        suggestion,
      }

      console.log(lugar)
      this.setState({lugar})
    } catch(err) {
      console.log(err)
    }
    //console.log('Selected suggestion:', suggestion)
    //this.setState({place: suggestion})
  }

  alCambiar = (e) => {
    this.setState({ [e.target.id]: e.target.value })
  }

  passwordsDiferentes = (e) => {
    // Custom validation por si los elementos no son iguales
    this.state.password !== e.target.value
      ? this.confirmPassword.setCustomValidity("Las contraseñas no son iguales")
      : this.confirmPassword.setCustomValidity('')
  }

  alEnviar = (e) => {
    e.preventDefault()
    this.props.clienteSignUp(this.state)
    console.log(this.state)
  }

  render() {
    const { classes, authError, role } = this.props
    const { isLoaded } = this.props.auth
    const weAreAuthorized = ['cliente', 'especialista', 'admin'].includes(role)

    return (
      <>
        { isLoaded && weAreAuthorized && <Redirect to='/clientes' /> }

        <FlexContainer className="full" itemsCenter justifyCenter directionColumn>
        <GridContainer justify="center">
          <GridItem xs={12} sm={6} md={4}>
          <form onSubmit={this.alEnviar}>
            <Card login>
              <CardHeader
                className={`${classes.cardHeader} ${classes.textCenter}`}
                color="info"
              >
               <h3 className={classes.cardTitle}>Nuevo cliente</h3>

              </CardHeader>
              <CardBody>
                <br/>

                <MUIPlacesAutocomplete
                  onSuggestionSelected={this.onSuggestionSelected}
                  renderTarget={() => (<div />)}
                  textFieldProps={{
                    placeholder: "Busque su clínica",
                    fullWidth: true,
                    required: true,
                  }}
                />

                <FormControl fullWidth>
                  <InputLabel htmlFor="email">Email</InputLabel>
                  <Input
                    id="email"
                    type="email"
                    onChange={this.alCambiar}
                    // eslint-disable-next-line
                    inputProps={{
                      pattern: "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{1,63}$"
                    }}
                    fullWidth
                    endAdornment={
                      <InputAdornment position="end">
                        <Email className={classes.inputAdornmentIcon} />
                      </InputAdornment>
                    }
                  />
                </FormControl>

                <FormControl fullWidth>
                  <InputLabel htmlFor="tel">Teléfono</InputLabel>
                  <Input
                    id="tel"
                    type="tel"
                    onChange={this.alCambiar}
                    // eslint-disable-next-line
                    inputProps={{
                      pattern: "^[0-9]{2,3}-? ?[0-9]{6,7}$",
                      minLength: 9
                    }}
                    inputRef={el => (this.phone = el)}
                    fullWidth
                    required
                    endAdornment={
                      <InputAdornment position="end">
                        <ContactPhone className={classes.inputAdornmentIcon} />
                      </InputAdornment>
                    }
                  />
                </FormControl>

                <FormControl fullWidth>
                  <InputLabel htmlFor="contra1">Contraseña</InputLabel>
                  <Input
                    id="password"
                    type="text"
                    onChange={this.alCambiar}
                    // eslint-disable-next-line
                    inputProps={{  }}
                    inputRef={el => (this.pass1 = el)}
                    fullWidth
                    required
                    endAdornment={
                      <InputAdornment position="end">
                        <LockOpen className={classes.inputAdornmentIcon} />
                      </InputAdornment>
                    }
                  />
                </FormControl>

                <FormControl fullWidth>
                  <InputLabel htmlFor="contra2">Confirmar contraseña</InputLabel>
                  <Input
                    id="confirmPassword"
                    type="text"
                    onChange={(e) => { this.alCambiar(e); this.passwordsDiferentes(e); }}
                    // eslint-disable-next-line
                    inputRef={el => (this.confirmPassword = el)}
                    fullWidth
                    required
                    endAdornment={
                      <InputAdornment position="end">
                        <Lock className={classes.inputAdornmentIcon} />
                      </InputAdornment>
                    }
                  />
                </FormControl>
              </CardBody>

              <CardFooter className={classes.justifyContentCenter}>
                <Button color="info" simple size="lg" block type="submit">
                  ¡Vamos!
                </Button>
              </CardFooter>
            </Card>
          </form>
          </GridItem>
        </GridContainer>

        { authError
          ? <CustomizedSnackbars message={authError} variant='warning' />
          : null
        }
      </FlexContainer>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError,
    auth: state.firebase.auth,
    role: state.auth.role,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clienteSignUp: (creds) => dispatch(clienteSignUp(creds)),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(loginPageStyle)
)(NuevoCliente)
