import React from 'react'
import GoogleButton from 'react-google-button'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { signInWithGAPI, signOut } from '../../store/actions/authActions'

import FlexContainer from 'react-styled-flexbox'

import withStyles from "@material-ui/core/styles/withStyles"
import GridContainer from "../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx"
import GridItem from "../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx"
import Card from "../../UI/material-dashboard-pro-react/src/components/Card/Card.jsx"
import CardBody from "../../UI/material-dashboard-pro-react/src/components/Card/CardBody.jsx"
import CardHeader from "../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"

import loginPageStyle from "../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx";

class GoogleSignIn extends React.Component {

  componentDidMount() {
    if(!window.gapi.auth2.getAuthInstance().isSignedIn.get()) return

    let token = window.gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token
    this.props.signInWithGAPI(token)
  }

  signIn = async () => {
    window.gapi.auth2.getAuthInstance().signIn()
  }

  render() {
    console.log(111, 'RENDER STATE', this.state)
    const { role, classes } = this.props
    if(role === 'especialista') return <Redirect to='/especialistas' />
    if(role === 'cliente') return <Redirect to='/clientes' />
    if(role === 'admin' || role === 'superAdmin') return <Redirect to='/admin' />

    else return (
      <FlexContainer className="full" itemsCenter justifyCenter directionColumn>
        <GridContainer justify="center">
          <GridItem xs={12} sm={6} md={4}>
            <Card login>
              <CardHeader
                className={`${classes.cardHeader} ${classes.textCenter}`}
                color="info"
              >
                <h3 className={classes.cardTitle}>Log in</h3>

              </CardHeader>
              <CardBody>
                <br/>
                <GoogleButton
                  label="¡Vamos allá!"
                  onClick={this.signIn}
                  style={{margin: '0 auto'}}/>
                <br/>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </FlexContainer>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    role: state.auth.role,
    cal: state.cal.cal
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signInWithGAPI: (token) => dispatch(signInWithGAPI(token)),
    signOut: () => dispatch(signOut()),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(loginPageStyle)
)(GoogleSignIn)

