import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { signInWithEmailAndPassword } from '../../store/actions/authActions'

import ForgotPassword from './ForgotPassword'

import FlexContainer from 'react-styled-flexbox'
import CustomizedSnackbars from '../UI/ComplexSnackbars'

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
import InputAdornment from '@material-ui/core/InputAdornment'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import FormControl from '@material-ui/core/FormControl'
import Typography from '@material-ui/core/Typography'

// @material-ui/icons
import Email from '@material-ui/icons/Email'
import RemoveRedEye  from '@material-ui/icons/RemoveRedEye'

// core components
import GridContainer from '../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx'
import GridItem from '../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx'
import Button from '../../UI/material-dashboard-pro-react/src/components/CustomButtons/Button.jsx'
import Card from '../../UI/material-dashboard-pro-react/src/components/Card/Card.jsx'
import CardBody from '../../UI/material-dashboard-pro-react/src/components/Card/CardBody.jsx'
import CardHeader from '../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx'
import CardFooter from '../../UI/material-dashboard-pro-react/src/components/Card/CardFooter.jsx'

import loginPageStyle from '../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx'

class PasswordInput extends Component {
  state = { passwordIsMasked: true }

  togglePasswordMask = () => {
    this.setState(prevState => ({
      passwordIsMasked: !prevState.passwordIsMasked,
    }))
  }

  render() {
    const { passwordIsMasked } = this.state
    return  <Input
              type={passwordIsMasked ? 'password' : 'text'}
              {...this.props}
              endAdornment={
                <InputAdornment style={{cursor: 'pointer'}} position="end">
                  <RemoveRedEye onClick={this.togglePasswordMask}/>
                </InputAdornment>
              }
            />
  }
}

class Login extends Component {
  state = {
    email: '',
    password: ''
  }

  alCambiar = (e) => {
    this.setState({ [e.target.id]: e.target.value })
  }

  alEnviar = (e) => {
    e.preventDefault()
    this.props.signInWithEmailAndPassword(this.state)
    console.log(this.state)
  }

  handleClose = e => {
    this.props.history.push('/login')
  }

  render() {
    const { classes, authError, logged, role } = this.props
    const forgotOpen = this.props.match.path.includes('forgot')
    const weAreAuthorized = (role === 'cliente' || role === 'especialista' || role === 'admin') ? true : false

    return (
      <>
        { logged && weAreAuthorized && <Redirect to='/clientes' /> }
        {forgotOpen && <ForgotPassword onClose={this.handleClose} />}

        <FlexContainer className="full" itemsCenter justifyCenter directionColumn>
        <GridContainer justify="center">
          <GridItem xs={12} sm={6} md={4}>
          <form onSubmit={this.alEnviar}>
            <Card login>
              <CardHeader
                className={`${classes.cardHeader} ${classes.textCenter}`}
                color="info"
              >
               <h3 className={classes.cardTitle}>Log in</h3>

              </CardHeader>
              <CardBody>
                <br/>
                <FormControl fullWidth>
                  <InputLabel htmlFor="email">Email</InputLabel>
                  <Input
                    id="email"
                    type="email"
                    onChange={this.alCambiar}
                    // eslint-disable-next-line
                    inputProps={{ pattern: "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" }}
                    inputRef={el => (this.email = el)}
                    fullWidth
                    endAdornment={
                      <InputAdornment position="end">
                        <Email className={classes.inputAdornmentIcon} />
                      </InputAdornment>
                    }
                  />
                </FormControl>

                <FormControl fullWidth>
                  <InputLabel htmlFor="password">Password</InputLabel>
                  <PasswordInput
                    id="password"
                    onChange={this.alCambiar}
                    fullWidth
                  />
                </FormControl>

                <Typography variant='caption' align='center'>
                  <Link to="/login/forgot-password" style={{ marginTop: 20 }} className={classes.perdiMiContra}>
                    Olvidé mi contraseña
                  </Link>
                </Typography>
              </CardBody>

              <CardFooter className={classes.justifyContentCenter}>
                <Button color="info" simple size="lg" block type="submit">
                  ¡Vamos!
                </Button>
              </CardFooter>
            </Card>
          </form>
          <Button variant="contained" href="/nuevo-cliente" color="info" size="lg" block type="button">
            Crear nueva cuenta
          </Button>
          </GridItem>
        </GridContainer>

        { authError
          ? <CustomizedSnackbars message={authError} variant='warning' />
          : null
        }
      </FlexContainer>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError,
    logged: state.auth.logged,
    auth: state.firebase.auth,
    role: state.auth.role,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signInWithEmailAndPassword: (creds) => dispatch(signInWithEmailAndPassword(creds)),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(loginPageStyle)
)(Login)
