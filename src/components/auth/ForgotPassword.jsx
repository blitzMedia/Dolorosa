import React from 'react'
import { firebaseConnect } from 'react-redux-firebase'
import FlexContainer from 'react-styled-flexbox'

import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import Email from '@material-ui/icons/Email'
import InputAdornment from '@material-ui/core/InputAdornment'
import CustomInput from "../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx"
import Button from "../../UI/material-dashboard-pro-react/src/components/CustomButtons/Button.jsx"

class ForgotPassword extends React.Component {

  state = {
    email: ''
  }

  alCambiar = (e) => {
    this.setState({ [e.target.id]: e.target.value })
  }

  alEnviar = (e) => {
    e.preventDefault()
    this.props.firebase.resetPassword(this.state.email)
    this.props.onClose()
  }

  render() {
    return (
      <Dialog open={true} aria-labelledby="simple-dialog-title">
        <FlexContainer itemsCenter justifyCenter directionColumn>
          <DialogTitle>
            Olvidé mi contraseña
          </DialogTitle>

          <form onSubmit={this.alEnviar} style={{ padding: 20, maxWidth: 320 }}>
            <CustomInput labelText="Email" id="email"
              formControlProps={{ fullWidth: true }}
              inputProps={{
                onChange: this.alCambiar,
                endAdornment: ( <InputAdornment position="end"><Email /></InputAdornment> )
              }}
            />
            <FlexContainer itemsCenter justifyCenter>
              <Button color="info" simple type="submit">Restablecer</Button>
              <Button color="secondary" simple onClick={() => this.props.onClose()}>Cancelar</Button>
            </FlexContainer>
          </form>
        </FlexContainer>
      </Dialog>
    )
  }
}

export default firebaseConnect()(ForgotPassword)
