import React from 'react'
import { Redirect } from 'react-router-dom'
import '../../../overapp.scss'
import { connect } from 'react-redux'
import { clienteSignUp } from '../../../store/actions/authActions'

import FlexContainer from 'react-styled-flexbox'

// core components
import Wizard from '../../../UI/material-dashboard-pro-react/src/components/Wizard/Wizard.jsx'
import GridContainer from '../../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx'
import GridItem from '../../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx'

import Step1 from './WizardSteps/Step1.jsx'
import Step2 from './WizardSteps/Step2.jsx'
import Step3 from './WizardSteps/Step3.jsx'

class WizardView extends React.Component {

  sendWiz = (state) => {
    console.log('FINAL STATE', state)
    this.props.clienteSignUp(state)
  }

  //Someday: datos fiscales
  render() {
    const { role = '' } = this.props
    const weAreAuthorized = (role === 'cliente' || role === 'especialista' || role === 'admin') ? true : false


    return (
      <FlexContainer className="full" itemsCenter justifyCenter>
        {weAreAuthorized && <Redirect to="/clientes" />}

        <GridContainer justify="center" style={{padding: '1em'}}>
          <GridItem xs={12} sm={9} lg={6}>
            <form>
              <Wizard
                steps={[
                  { stepName: "Centro", stepComponent: Step1, stepId: "centro" },
                  { stepName: "Dirección", stepComponent: Step2, stepId: "direccion" },
                  { stepName: "Contacto", stepComponent: Step3, stepId: "contacto" }
                ]}
                title="Bienvenid@ a Indolora"
                subtitle="Nuevo cliente"
                previousButtonText="Anterior"
                nextButtonText="Siguiente"

                finishButtonClick={this.sendWiz}
              />
            </form>
          </GridItem>
        </GridContainer>
      </FlexContainer>
    )
  }
}

const mapStateToProps = (state) => {
  console.log('Nuevo Usuario', state)
  return {
    auth: state.firebase.auth,
    role: state.auth.role
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clienteSignUp: (creds) => dispatch(clienteSignUp(creds))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WizardView)
