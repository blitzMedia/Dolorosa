import React from 'react'

import FlexContainer from 'react-styled-flexbox'

// @material-ui/icons
import LocalHospital from '@material-ui/icons/LocalHospital'
import Email from '@material-ui/icons/Email'
import LockOutlined from '@material-ui/icons/LockOutlined'
import LockOpen from '@material-ui/icons/LockOpen'

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
import InputAdornment from '@material-ui/core/InputAdornment'
import Typography from '@material-ui/core/Typography'

// core components
import CustomInput from '../../../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx'

const style = {
  infoText: {
    fontWeight: "300",
    margin: "10px 0 30px",
    textAlign: "center"
  },
  inputAdornmentIcon: {
    color: "#555"
  },
  inputAdornment: {
    position: "relative"
  }
};

class Step1 extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      nombreCentro: '',
      nombreCentroState: '',
      email: '',
      emailState: '',
      password: '',
      passwordState: '',
      confirmPassword: '',
      confirmPasswordState: ''
    }
  }
  sendState() {
    return this.state;
  }

  // function that returns true if value is email, false otherwise
  verifyEmail(value) {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  }
  // function that verifies if a string has a given length or not
  verifyLength(value, length) {
    if (value.length >= length) {
      return true;
    }
    return false;
  }
  verifyPass(pass) {
    const boo = pass === this.state.password
                ? true
                : false
    return boo
  }
  change(event, stateName, type, stateNameEqualTo) {
    switch (type) {
      case "email":
        if (this.verifyEmail(event.target.value)) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;

      case "length":
        if (this.verifyLength(event.target.value, stateNameEqualTo)) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;

      case "confirmPassword":
        const passPassed = this.verifyPass(event.target.value) ? 'success' : 'error'
        this.setState({ [stateName + "State"]: passPassed })
        break;

      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  }
  isValidated() {
    if (
      this.state.nombreCentroState === "success" &&
      this.state.emailState === "success" &&
      this.state.passwordState === "success" &&
      this.state.confirmPasswordState === "success"
    ) {
      return true;
    } else {
      if (this.state.nombreCentroState !== "success") {
        this.setState({ nombreCentroState: "error" });
      }
      if (this.state.emailState !== "success") {
        this.setState({ emailState: "error" });
      }
      if (this.state.passwordState !== "success") {
        this.setState({ passwordState: "error" });
      }
      if (this.state.confirmPasswordState !== "success") {
        this.setState({ confirmPasswordState: "error" });
      }
    }
    return false;
  }
  render() {
    const { classes } = this.props;
    return (
      <section>
        <FlexContainer directionColumn itemsCenter justifyCenter>
          <Typography paragraph>
            Empecemos con la información básica del centro
          </Typography>

          <br />

          <CustomInput
            success={this.state.nombreCentroState === "success"}
            error={this.state.nombreCentroState === "error"}
            labelText={
              <span>
                Nombre del centro
              </span>
            }
            id="nombreCentro"
            formControlProps={{
              fullWidth: true
            }}
            inputProps={{
              onChange: event => this.change(event, "nombreCentro", "length", 3),
              endAdornment: (
                <InputAdornment
                  position="end"
                  className={classes.inputAdornment}
                >
                  <LocalHospital className={classes.inputAdornmentIcon} />
                </InputAdornment>
              )
            }}
          />

          <CustomInput
            success={this.state.emailState === "success"}
            error={this.state.emailState === "error"}
            labelText={
              <span>
                Email del Centro
              </span>
            }
            id="email"
            formControlProps={{
              fullWidth: true
            }}
            inputProps={{
              onChange: event => this.change(event, "email", "email"),
              endAdornment: (
                <InputAdornment
                  position="end"
                  className={classes.inputAdornment}
                >
                  <Email className={classes.inputAdornmentIcon} />
                </InputAdornment>
              )
            }}
          />

          <CustomInput
            success={this.state.passwordState === "success"}
            error={this.state.passwordState === "error"}
            labelText={
              <span>
                Contraseña <small>(requerido)</small>
              </span>
            }
            id="password"
            formControlProps={{
              fullWidth: true
            }}
            inputProps={{
              onChange: event => this.change(event, "password", "length", 8),
              endAdornment: (
                <InputAdornment
                  position="end"
                  className={classes.inputAdornment}
                >
                  <LockOpen className={classes.inputAdornmentIcon} />
                </InputAdornment>
              )
            }}
          />

          <CustomInput
            success={this.state.confirmPasswordState === "success"}
            error={this.state.confirmPasswordState === "error"}
            labelText={
              <span>
                Confirmar Contraseña <small>(requerido)</small>
              </span>
            }
            id="password"
            formControlProps={{
              fullWidth: true
            }}
            inputProps={{
              onChange: event => {
                this.change(event, "confirmPassword", "confirmPassword");
              },
              ref: el => {this.confirmPassword = el},
              endAdornment: (
                <InputAdornment
                  position="end"
                  className={classes.inputAdornment}
                >
                  <LockOutlined className={classes.inputAdornmentIcon} />
                </InputAdornment>
              )
            }}
          />
        </FlexContainer>
      </section>
    )
  }
}

export default withStyles(style)(Step1);
