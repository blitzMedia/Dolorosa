import React from 'react'

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
import CustomInput from '../../../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx'
import Typography from '@material-ui/core/Typography'
import InputAdornment from '@material-ui/core/InputAdornment'
import Email from '@material-ui/icons/Email'
import Face from '@material-ui/icons/Face'
import Smartphone from '@material-ui/icons/Smartphone'

import customSelectStyle from '../../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/customSelectStyle.jsx'
import customCheckboxRadioSwitch from '../../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/customCheckboxRadioSwitch.jsx'

const style = {
  infoText: {
    fontWeight: "300",
    margin: "10px 0 30px",
    textAlign: "center"
  },
  inputAdornmentIcon: {
    color: "#555"
  },
  choiche: {
    textAlign: "center",
    cursor: "pointer",
    marginTop: "20px"
  },
  ...customSelectStyle,
  ...customCheckboxRadioSwitch
};

class Step3 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contacto: "",
      contactoState: "",
      tlf: "",
      tlfState: "",
      email: "",
      emailState: ""
    };
  }
  sendState() {
    return this.state;
  }

  // function that returns true if value is email, false otherwise
  verifyEmail(value) {
    var emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRex.test(value)) {
      return true;
    }
    return false;
  }

  // function that verifies if a string has a given length or not
  verifyLength(value, length) {
    if (value.length >= length) {
      return true;
    }
    return false;
  }
  change(event, stateName, type, stateNameEqualTo) {
    switch (type) {
      case "email":
        if (this.verifyEmail(event.target.value)) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;
      case "length":
        if (this.verifyLength(event.target.value, stateNameEqualTo)) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  }
  isValidated() {
    if (
      this.state.contactoState === "success" &&
      this.state.tlfState === "success"
      //&& this.state.emailState === "success"
    ) {
      return true;
    } else {
      if (this.state.contactoState !== "success") {
        this.setState({ contactoState: "error" });
      }
      if (this.state.tlfState !== "success") {
        this.setState({ tlfState: "error" });
      }
      if (this.state.emailState !== "success") {
        this.setState({ emailState: "error" });
      }
    }
    return false;
  }
  render() {
    const { classes } = this.props;
    return (
      <section style={{ textAlign: 'center' }}>
        <Typography paragraph>Persona de contacto</Typography>
        <CustomInput
          success={this.state.contactoState === "success"}
          error={this.state.contactoState === "error"}
          labelText={
            <span>
              Nombre completo <small>(requerido)</small>
            </span>
          }
          id="contacto"
          formControlProps={{
            fullWidth: true
          }}
          inputProps={{
            onChange: event => this.change(event, "contacto", "length", 7),
            endAdornment: (
              <InputAdornment
                position="end"
                className={classes.inputAdornment}
              >
                <Face className={classes.inputAdornmentIcon} />
              </InputAdornment>
            )
          }}
        />
        <CustomInput
          success={this.state.tlfState === "success"}
          error={this.state.tlfState === "error"}
          labelText={
            <span>
              Teléfono <small>(requerido)</small>
            </span>
          }
          id="tlf"
          formControlProps={{
            fullWidth: true
          }}
          inputProps={{
            onChange: event => this.change(event, "tlf", "length", 9),
            endAdornment: (
              <InputAdornment
                position="end"
                className={classes.inputAdornment}
              >
                <Smartphone className={classes.inputAdornmentIcon} />
              </InputAdornment>
            )
          }}
        />
        <CustomInput
          success={this.state.emailState === "success"}
          error={this.state.emailState === "error"}
          labelText={
            <span>
              Email de la persona de contacto
            </span>
          }
          id="email"
          formControlProps={{
            fullWidth: true
          }}
          inputProps={{
            onChange: event => this.change(event, "email", "email"),
            endAdornment: (
              <InputAdornment
                position="end"
                className={classes.inputAdornment}
              >
                <Email className={classes.inputAdornmentIcon} />
              </InputAdornment>
            )
          }}
        />
      </section>
    );
  }
}

export default withStyles(style)(Step3)
