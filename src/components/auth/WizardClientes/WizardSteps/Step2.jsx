import React from 'react';

// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
import Typography from '@material-ui/core/Typography'

// core components
import CustomInput from '../../../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx'
import GridContainer from '../../../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx'
import GridItem from '../../../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx'

import customSelectStyle from '../../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/customSelectStyle.jsx'

const style = {
  infoText: {
    fontWeight: "300",
    margin: "10px 0 30px",
    textAlign: "center"
  },
  ...customSelectStyle
};

class Step2 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      calle: "",
      calleState: "",
      num: "",
      numState: "",
      ciudad: "",
      ciudadState: "",
      provincia: "",
      provinciaState: "",
      cp: "",
      cpState: ""
    };
  }
  sendState() {
    return this.state;
  }

  // function that verifies if a string has a given length or not
  verifyLength(value, length) {
    if (value.length >= length) {
      return true;
    }
    return false;
  }

  change(event, stateName, type, stateNameEqualTo) {
    switch (type) {
      case "length":
        if (this.verifyLength(event.target.value, stateNameEqualTo)) {
          this.setState({ [stateName + "State"]: "success" });
        } else {
          this.setState({ [stateName + "State"]: "error" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  }

  isValidated() {
    if (
      this.state.calleState === "success" &&
      this.state.numState === "success" &&
      this.state.ciudadState === "success" &&
      this.state.provinciaState === "success" &&
      this.state.cpState === "success"
    ) {
      return true;
    } else {
      if (this.state.calleState !== "success") {
        this.setState({ calleState: "error" });
      }
      if (this.state.numState !== "success") {
        this.setState({ numState: "error" });
      }
      if (this.state.ciudadState !== "success") {
        this.setState({ ciudadState: "error" });
      }
      if (this.state.cpState !== "success") {
        this.setState({ cpState: "error" });
      }
    }
    return false;
  }

  render() {
    return (
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} style={{ textAlign: 'center' }}>
          <Typography paragraph>Dirección del Centro</Typography>
        </GridItem>
        <GridItem xs={12} sm={7}>
          <CustomInput
            success={this.state.calleState === "success"}
            error={this.state.calleState === "error"}
            inputProps={{
              onChange: event => this.change(event, "calle", "length", 5),
            }}
            labelText="Nombre de la calle"
            id="calle"
            formControlProps={{
              fullWidth: true
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={3}>
          <CustomInput
            success={this.state.numState === "success"}
            error={this.state.numState === "error"}
            inputProps={{
              //type:"number",
              onChange: event => this.change(event, "num", "length", 1),
            }}
            labelText="Número"
            id="num"
            formControlProps={{
              fullWidth: true
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={3}>
          <CustomInput
            success={this.state.ciudadState === "success"}
            error={this.state.ciudadState === "error"}
            inputProps={{
              onChange: event => this.change(event, "ciudad", "length", 3),
            }}
            labelText="Ciudad"
            id="ciudad"
            formControlProps={{
              fullWidth: true
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={4}>
          <CustomInput
            success={this.state.provinciaState === "success"}
            error={this.state.provinciaState === "error"}
            inputProps={{
              onChange: event => this.change(event, "provincia", "length", 3),
            }}
            labelText="Provincia"
            id="provincia"
            formControlProps={{
              fullWidth: true
            }}
          />
        </GridItem>
        <GridItem xs={12} sm={3}>
          <CustomInput
            success={this.state.cpState === "success"}
            error={this.state.cpState === "error"}
            inputProps={{
              onChange: event => this.change(event, "cp", "length", 5),
            }}
            labelText="Código Postal"
            id="cp"
            formControlProps={{
              fullWidth: true
            }}
          />
        </GridItem>
      </GridContainer>
    );
  }
}

export default withStyles(style)(Step2);
