import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { adminSignUp } from '../../store/actions/authActions'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';

const tiposDeUsuario = [
  { value: '', label: 'Tipo de usuario' },
  { value: 'admin', label: 'Admin' },
  { value: 'especialista', label: 'Especialista' },
  { value: 'cliente', label: 'Cliente' }
]

class NuevoUsuario extends Component {
  constructor(props) {
    super(props)

    this.confirmPassword = React.createRef()
    this.state = {
      type: '',
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: ''
    }
  }
  //if (!auth.uid) return <Redirect to='/signin' />

  alCambiar = (e) => {
    this.setState({ [e.target.id]: e.target.value })
  }

  alEnviar = (e) => {
    e.preventDefault()
    this.props.adminSignUp(this.state)
  }

  passwordsDiferentes = (e) => {
    // Custom validation por si los elementos no son iguales
    this.state.password !== e.target.value
      ? this.confirmPassword.setCustomValidity("Las contraseñas no son iguales")
      : this.confirmPassword.setCustomValidity('')
  }

  render() {
    const { auth, authError, role } = this.props

    if (!auth.uid) return <Redirect to='/login' />
    if(role !== 'admin') return <Redirect to='/' />

    const defaultType = tiposDeUsuario[0].value

    const todosLosTipos = tiposDeUsuario &&
      tiposDeUsuario.map(opt => (
        <option key={opt.value} value={opt.value}>{opt.label}</option>)
      )



    return (
      <section>
        <Typography variant='h5' align='center' gutterBottom>Nuevo usuario</Typography>
        <br/>

        <form className="vertical" onSubmit={this.alEnviar}>
          <select id="type"
                  onChange={this.alCambiar}
                  defaultValue={defaultType}>
            {todosLosTipos}
          </select>

          <input required id="firstName" type="text" placeholder="Nombre" onChange={this.alCambiar} />
          <input required id="lastName" type="text" placeholder="Apellidos" onChange={this.alCambiar} />
          <input required id="email" type="text" placeholder="Email" onChange={this.alCambiar} />
          <input required id="password" type="password" placeholder="Contraseña" onChange={this.alCambiar} />
          <input required id="confirmPassword" type="password" placeholder="Repetir contraseña"
            onChange={(e) => { this.alCambiar(e); this.passwordsDiferentes(e); }}
            ref={el => this.confirmPassword = el}
          />

          <Button color="primary" type="submit">Nuevo usuario</Button>

          { authError ? <p>{authError}</p> : null }

        </form>

      </section>
    )
  }
}

const mapStateToProps = (state) => {
  console.log('Nuevo Usuario', state)
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
    role: state.firebase.profile.role
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    adminSignUp: (newUser) => dispatch(adminSignUp(newUser))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NuevoUsuario)
