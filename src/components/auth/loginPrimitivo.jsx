import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux'
import { signInWithEmailAndPassword } from '../../store/actions/authActions'
import CustomizedSnackbars from '../UI/ComplexSnackbars'


class Login extends Component {

  state = {
    email: '',
    password: ''
  }

  alCambiar = (e) => {
    this.setState({ [e.target.id]: e.target.value })
  }

  alEnviar = (e) => {
    e.preventDefault()
    this.props.signInWithEmailAndPassword(this.state)
  }

  render() {
    const { authError, auth } = this.props
    if (auth.uid) return <Redirect to='/' />

    return (
      <>
        <section>
          <form className="vertical" onSubmit={this.alEnviar}>
            <input id="email" type="text" placeholder="usuario" onChange={this.alCambiar} />
            <input id="password" type="password" placeholder="contraseña" onChange={this.alCambiar} />
            <Button color="primary" type="submit">Login</Button>
          </form>

          { authError
            ? <CustomizedSnackbars message={authError} variant='warning' />
            : null
          }

        </section>
      </>
    )
  }
}

//<CustomizedSnackbars message='Estás logueado!' variant='success' />

const mapStateToProps = (state) => {
  return {
    authError: state.auth.authError,
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signInWithEmailAndPassword: (creds) => dispatch(signInWithEmailAndPassword(creds))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
