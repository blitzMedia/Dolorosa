import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { signInWithGAPI } from '../../store/actions/authActions'

import FlexContainer from 'react-styled-flexbox'
import { Cube } from 'styled-loaders-react'
import YoureLost from '../../graphics/undraw_not_found.svg'
import { MiniImg } from '../../things/StyledComponents'

class GetOut extends React.Component {
  state = {
    message: false,
    out: false
  }

  componentDidMount() {
    setTimeout(() => { this.setState({out: true})}, 3000)
  }

  render() {
    return (
      <FlexContainer directionColumn itemsCenter justifyCenter>
        <MiniImg src={YoureLost} alt="You're lost"/>
        <h3>No tienes permiso para acceder a nuestra aplicación.</h3>
        <p>Gracias, pero no gracias!</p>
        { this.state.out && <Redirect to='/' /> }
      </FlexContainer>
    )
  }
}

class AutoSignIn extends React.Component {

  componentDidMount() {
    if(!window.gapi.auth2.getAuthInstance().isSignedIn.get()) {
      window.gapi.auth2.getAuthInstance().signIn()
    }
    else {
      let token = window.gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token
      this.props.signInWithGAPI(token)
    }
  }

  render() {
    const { role, youreout = false } = this.props.auth
    if(role === 'admin' || role === 'superAdmin') return <Redirect to='/admin' />

    // Si encontramos el rol, redirigimos
    if(role === 'especialista') return <Redirect to='/especialistas' />

    // Si no, cargamos el loader
    return (
      <div className="superCenter">
        { youreout && <GetOut /> }
        <Cube size="60px" color="#09837d" />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { auth: state.auth }
}

const mapDispatchToProps = (dispatch) => {
  return { signInWithGAPI: (token) => dispatch(signInWithGAPI(token)) }
}

export default connect(mapStateToProps, mapDispatchToProps)(AutoSignIn)

