import React from 'react'
import { connect } from 'react-redux'

import { updateEvent, deleteEvent } from '../../../store/actions/eventActions'

import Button from '@material-ui/core/Button'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Today from "@material-ui/icons/Today"

import GridContainer from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx"
import GridItem from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx"

import CardHeader from "../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"
import CardText from "../../../UI/material-dashboard-pro-react/src/components/Card/CardText.jsx"

import Datetime from "react-datetime"
import moment from "moment"

class ModalEvento extends React.Component {
  state = null

  componentDidMount = () => {
    this.setState(this.props.event)
  }

  onSubmit = (e) => {
    console.log(e)
  }

  onChangeStart = (ev) => {
    const start = ev.toDate()
    this.setState({start})
  }
  onChangeEnd = (ev) => {
    const end = ev.toDate()
    this.setState({end})
  }
  guardar = ev => {
    this.props.updateEvent(this.state)
    this.props.onClose()
  }
  eliminar = ev => {
    this.props.deleteEvent(this.state)
    this.props.onClose()
    //this.setState({open: false})
  }

  render() {
    const {start, end, title } = this.state || ''
    const fecha = moment(start).format('LL')
    const minMaxDatetime = { minutes: { step: 30 } }

    return (
      <div style={{ overflow: 'visible' }}>
        <form onSubmit={this.onSubmit}>
          <CardHeader icon>
            <CardText className="iconHeader greenHeader">
              <Today />
              <h4>{fecha} - {title}</h4>
            </CardText>
          </CardHeader>
          <DialogContent style={{ overflow: 'visible', paddingTop: 40 }}>

            <GridContainer justify="center" style={{ alignItems: 'center' }}>
              <GridItem xs={6} sm={6} md={6}>
                <Datetime
                  id="date-start"
                  value={start}
                  dateFormat={false}
                  inputProps={{ placeholder: "Inicio" }}
                  onChange={this.onChangeStart}
                  timeConstraints={minMaxDatetime}
                />
              </GridItem>

              <GridItem xs={6} sm={6} md={6}>
                <Datetime
                  id="date-end"
                  value={end}
                  dateFormat={false}
                  inputProps={{ placeholder: "Fin" }}
                  onChange={this.onChangeEnd}
                  timeConstraints={minMaxDatetime}
                />
              </GridItem>
            </GridContainer>
          </DialogContent>

          <DialogActions style={{ justifyContent: 'space-between' }}>
            <div>
              <Button color="secondary" variant="contained" onClick={this.props.onClose}>
                Cancelar
              </Button>
            </div>
            <div>
              {
                !this.props.event.summary.includes('lock')
                  ? (
                    <>
                      <Button color="secondary" onClick={this.eliminar}>
                        Eliminar
                      </Button>
                      <Button color="primary" style={{marginLeft: 20}} onClick={this.guardar}>
                        Guardar
                      </Button>
                    </>
                  )
                  : (
                    <Button color="primary" disabled style={{marginLeft: 20}} onClick={this.guardar}>
                      Bloqueado
                    </Button>
                  )
              }
            </div>
          </DialogActions>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateEvent: (event) => dispatch(updateEvent(event)),
    deleteEvent: (event) => dispatch(deleteEvent(event))
  }
}

export default connect(null, mapDispatchToProps)(ModalEvento)
