import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import Divider from '@material-ui/core/Divider'

const UserSummary = ({user}) => {
  return (
    <>
      <ListItem>
        <ListItemText primary={user.displayName} />

        <ListItemSecondaryAction>
          <Button variant="fab" mini color="secondary" aria-label="Add">
            <AddIcon />
          </Button>
        </ListItemSecondaryAction>
      </ListItem>
      <Divider />
    </>
  )
}

export default UserSummary
