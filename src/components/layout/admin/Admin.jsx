import React from "react"
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import CalAdmin from '../../calendar/CalAdmin'

const Admin = (props) => {
  console.log(props.auth)

  return (
    <>
    {
      props.auth.isEmpty ? <Redirect to='/' /> : <CalAdmin />
    }
    </>
  )
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    role: state.auth.role
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    //signOut: () => dispatch(signOut())
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(Admin)
