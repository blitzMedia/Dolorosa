import React, { Component } from 'react'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect } from 'react-router-dom'
//import CitasLista from '../../citas/CitasLista'
import ProjectsList from '../../test/ProjectsList'
import CalList from '../../test/CalList'
import UserList from './UserList'
import Typography from '@material-ui/core/Typography'

//<CitasLista citas={citas} />
class Dashboard extends Component {
  render() {
    const { auth, profile, projects, users, role } = this.props
    if (!auth.uid) return <Redirect to='/login' />

    return (
      <section style={{ textAlign: 'center' }}>
        <Typography variant='h5' align='center' gutterBottom>Bienvenid@, {profile.displayName}!</Typography>
        <p style={{textAlign: 'center'}}>Eres un gran {role}</p>

        {
          role === 'admin' && <UserList users={users} />
        }
        {
          role === 'admin' && <ProjectsList projects={projects} />
        }
        {
          role === 'especialista' && <CalList />
        }
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  //console.log('Dashboard', state)
  return {
    citas: state.citas.citas,
    projects: state.firestore.ordered.projects,
    users: state.firestore.ordered.users,
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    role: state.firebase.profile.role
  }
}


export default compose(
  connect(mapStateToProps),
  firestoreConnect([
    { collection: 'projects' },
    { collection: 'users' }
  ])
)(Dashboard)
