import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { signOut } from '../../../../store/actions/authActions'

import CalAdmin from '../../../calendar/CalAdmin'

class Citas extends Component {
  render() {
    const { auth } = this.props
    if (!auth.uid) return <Redirect to='/' />

    return (
      <>
        <CalAdmin />
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut()),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(Citas)
