import React from 'react'
import { connect } from 'react-redux'

import { citaToAdminDB } from '../../../../store/actions/citasActions'

//import { formatEventsForCalendar, listEventsById } from '../../../../calendar/calendarHelpers'

import Button from '@material-ui/core/Button'
//import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import FormLabel from "@material-ui/core/FormLabel"

import FormControlLabel from "@material-ui/core/FormControlLabel"
import Checkbox from "@material-ui/core/Checkbox"
import Check from "@material-ui/icons/Check"
import Today from "@material-ui/icons/Today"
import Slider from '@material-ui/lab/Slider'

import CustomInput from "../../../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx"
import GridContainer from "../../../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx"
import GridItem from "../../../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx"
import CardHeader from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"
import CardText from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardText.jsx"

import Datetime from "react-datetime"
import moment from "moment"

class ModalCitaAdmin extends React.Component {
  state = {
    slot: {
      start: '',
      end: ''
    },
    cita: {
      checkedEspeciales: false,
      duracion: 60
    }
  }

  componentWillMount() {
    const { slot: {start, end} } = this.props
    this.setState({slot: {start: moment(start), end: moment(end)}})
  }

  onSubmit = (e) => {
    e.preventDefault()
    console.log(this.state.cita)
    this.props.citaToAdminDB(this.state.cita)
  }

  handleChange = (e) => {
    const { cita } = this.state
    this.setState({cita: {
      ...cita,
      [e.target.id]: e.target.value
    }})
  }

  onChangeSlot = (slot) => {
    const { slot: {end}, cita } = this.state
    this.setState({
      slot: {start: slot, end},
      ...cita,
      cita: { start: slot, end: end }
    })
  }

  handleCheck = e => {
    const { cita } = this.state
    this.setState({ cita: {
      ...cita,
      checkedEspeciales: e.target.checked
    }})
  }

  handleSlider = (event, value) => {
    this.setState({cita: { duracion: value }});
  }

  render() {
    const { slot: { start, end } } = this.props
    const startDate = moment(start)
    const endDate = moment(end)

    const { duracion } = this.state.cita

    // Time constraints
    const minMaxDatetime = {
      minutes: { step: 30 },
      hours: { min: startDate.hours(), max: endDate.hours(), step: 1 }
    }

    return (
      <Dialog
        open={true}
        fullWidth
        className="modalConDatepicker"
      >
        <form onSubmit={this.onSubmit}>
          <CardHeader color="rose" icon>
            <CardText color="rose" className="iconHeader greenHeader">
              <Today />
              <h4>Nueva cita - {startDate.format('LL')}</h4>
            </CardText>
          </CardHeader>
          <DialogContent style={{ overflow: 'visible', marginTop: 20 }}>
            <Datetime
              id="date-start"
              defaultValue={this.state.slot.start}
              dateFormat={false}
              inputProps={{
                'placeholder': 'Inicio',
                'required': true
              }}
              onChange={slot => this.onChangeSlot(slot)}
              timeConstraints={minMaxDatetime}
              isValidDate={this.isValid}
            />
            <CustomInput
              labelText="Nombre del paciente"
              id="nombre_paciente"
              formControlProps={{fullWidth: true}}
              inputProps={{
                onChange: this.handleChange,
                required: true
              }}
            />
            <CustomInput
              labelText="Teléfono del paciente"
              id="tel_paciente"
              formControlProps={{fullWidth: true}}
              inputProps={{
                type: "number",
                onChange: this.handleChange,
                required: true
              }}
            />
            <CustomInput
              labelText="Procedimiento"
              id="procedimiento"
              formControlProps={{fullWidth: true}}
              inputProps={{
                onChange: this.handleChange,
                required: true
              }}
            />

            <GridContainer style={{
              marginTop: 30,
              marginBottom: 10,
              display: 'flex',
              alignItems: 'center'
            }}>
            <GridItem xs={12} sm={4}>
              <FormLabel className="labelSlider">
                Duración estimada: { duracion }'
              </FormLabel>
            </GridItem>
            <GridItem xs={12} sm={8}>
              <Slider
                id="procedimiento_duracion"
                style={{ padding: '10px 0px' }}
                value={duracion}
                min={0}
                max={150}
                step={30}
                onChange={this.handleSlider}
              />
            </GridItem>
          </GridContainer>

          <CustomInput
            labelText="Observaciones"
            id="observaciones"
            formControlProps={{fullWidth: true}}
            inputProps={{
              multiline: true,
              onChange: this.handleChange,
              required: true
            }}
          />
          <FormControlLabel
            control={
              <Checkbox
                tabIndex={-1}
                checked={this.state.checkedEspeciales}
                onClick={e => this.handleCheck(e)}
                checkedIcon={<Check style={{
                  width: "20px",
                  height: "20px",
                  border: "1px solid rgba(0, 0, 0, .54)",
                  borderRadius: "3px",
                  color: "black"
                }} />}
                icon={<Check style={{
                  width: "0px",
                  height: "0px",
                  padding: "9px",
                  border: "1px solid rgba(0, 0, 0, .54)",
                  borderRadius: "3px"
                }} />}
              />
            }
            label="Niño o necesidades especiales"
          />
          </DialogContent>

          <DialogActions>
            <Button color="primary" onClick={this.props.onClose}>
              Cancelar
            </Button>
            <Button color="primary" type="submit">
              Guardar
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    citaToAdminDB: (cita) => dispatch(citaToAdminDB(cita))
  }
}

const mapStateToProps = (state) => {
  console.log('Fetch', state)
  return {
    cal: state.cal
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModalCitaAdmin)
