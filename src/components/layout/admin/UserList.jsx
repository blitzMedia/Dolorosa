import React from 'react'
import List from '@material-ui/core/List';
import UserSummary from './UserSummary'

const UserList = ({users}) => {
  console.log(users)
  return (
    <List>
      { users && users.map(user => <UserSummary key={user.id} user={user} />) }
    </List>
  )
}

export default UserList
