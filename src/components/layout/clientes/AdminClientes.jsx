import React from "react"
import Citas from './widgets/Citas'

// core components
import GridContainer from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx"
import GridItem from "../../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx"

class AdminClientes extends React.Component {
  render() {

    return (
      <>
        <GridContainer className="General">
          <GridItem xs={12} sm={12} md={10}>
            <Citas />
          </GridItem>
        </GridContainer>
      </>
    )
  }
}

export default AdminClientes;

