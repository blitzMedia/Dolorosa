import React, { Component } from 'react'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// material-ui icons
import Assignment from "@material-ui/icons/Assignment";
import Person from "@material-ui/icons/Person";
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";

// core components
import Card from "../../../../UI/material-dashboard-pro-react/src/components/Card/Card.jsx";
import CardHeader from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx";
import CardIcon from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardIcon.jsx";
import CardBody from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardBody.jsx";
import Table from "../../../../UI/material-dashboard-pro-react/src/components/Table/Table.jsx";
import Button from "../../../../UI/material-dashboard-pro-react/src/components/CustomButtons/Button.jsx";


import extendedTablesStyle from "../../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/views/extendedTablesStyle.jsx";


class CitaPropuesta extends Component {
  render () {
    const { classes } = this.props;
    const simpleButtons = [
      { color: "info", icon: Person },
      { color: "success", icon: Edit },
      { color: "danger", icon: Close }
    ].map((prop, key) => {
      return (
        <Button
          color={prop.color}
          simple
          className={classes.actionButton}
          key={key}
        >
          <prop.icon className={classes.icon} />
        </Button>
      );
    });

    return (
      <Card style={{marginTop: 20}}>
        <CardHeader color="rose" icon>
          <CardIcon color="rose">
            <Assignment />
          </CardIcon>
          <h4 className={classes.cardIconTitle}>Citas propuestas</h4>
        </CardHeader>
        <CardBody>
          <Table
            tableHead={[
              "#",
              "Nombre",
              "Actuación",
              "Fecha propuesta",
              "Acciones"
            ]}
            tableData={[
              [
                "1",
                "José Ángel Palomo",
                "Tornillo en la muela",
                "15 de agosto a las 14:30",
                simpleButtons
              ],
              ["2", "Gorka", "Nueva dentadura", "15 de agosto a las 14:30", simpleButtons],
              [
                "3",
                "Cárol Órdoñez",
                "Dentadura postiza pro",
                "15 de agosto a las 14:30",
                simpleButtons
              ],
              [
                "4",
                "Silvia Fernández",
                "Caries múltiples",
                "15 de agosto a las 14:30",
                simpleButtons
              ],
              [
                "5",
                "Paul Dickens",
                "Limpieza radical",
                "15 de agosto a las 14:30",
                simpleButtons
              ]
            ]}
            customCellClasses={[
              classes.center,
              classes.right,
              classes.right
            ]}
            customClassesForCells={[0, 4, 5]}
            customHeadCellClasses={[
              classes.center,
              classes.right,
              classes.right
            ]}
            customHeadClassesForCells={[0, 4, 5]}
          />
        </CardBody>
      </Card>
    )
  }
}

export default withStyles(extendedTablesStyle)(CitaPropuesta)
