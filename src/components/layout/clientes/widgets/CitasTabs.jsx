import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { signOut } from '../../../../store/actions/authActions'

import NuevaCitaClientes from './NuevaCitaClientes'
import CitaPropuesta from './CitaPropuesta'
import CitaAprobada from './CitaAprobada'
import CalClientes from '../../../calendar/CalClientes'


// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"


// @material-ui/icons
import Today from "@material-ui/icons/Today"
import Info from "@material-ui/icons/Info"
import LocationOn from "@material-ui/icons/LocationOn"

// core components
import NavPills from "../../../../UI/material-dashboard-pro-react/src/components/NavPills/NavPills.jsx";

import { cardTitle } from "../../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react.jsx";


const styles = {
  cardTitle,
  pageSubcategoriesTitle: {
    color: "#3C4858",
    textDecoration: "none",
    textAlign: "center"
  },
  cardCategory: {
    margin: "0",
    color: "#999999"
  }
};

class Citas extends Component {
  render() {
    const { auth } = this.props
    if (!auth.uid) return <Redirect to='/' />

    return (
      <NavPills
        color="rose"
        alignLeft
        horizontal={{
          tabsGrid: { xs: 12, md: 2 },
          contentGrid: { xs: 12, md: 10 }
        }}
        tabs={[

          {
            tabButton: "Calendario",
            tabIcon: Today,
            tabContent: (
              <CalClientes />
            )
          },{
            tabButton: "Nueva Cita",
            tabIcon: Today,
            tabContent: (
              <NuevaCitaClientes />
            )
          },{
            tabButton: "Citas propuestas",
            tabIcon: Info,
            tabContent: (
              <CitaPropuesta />
            )
          },{
            tabButton: "Citas aprobadas",
            tabIcon: LocationOn,
            tabContent: (
              <CitaAprobada />
              )
          }
        ]}
      />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut()),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles)
)(Citas)
