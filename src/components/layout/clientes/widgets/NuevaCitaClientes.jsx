import React, { Component } from 'react'

// react component plugin for creating a beautiful datetime dropdown picker
import Datetime from "react-datetime"

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
import FormControl from "@material-ui/core/FormControl"

// @material-ui/icons
import Today from "@material-ui/icons/Today"

// core components
import Card from "../../../../UI/material-dashboard-pro-react/src/components/Card/Card.jsx"
import CardHeader from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardHeader.jsx"
import CardFooter from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardFooter.jsx"
import CardIcon from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardIcon.jsx"
import CardBody from "../../../../UI/material-dashboard-pro-react/src/components/Card/CardBody.jsx"
import Button from "../../../../UI/material-dashboard-pro-react/src/components/CustomButtons/Button.jsx"
import CustomInput from "../../../../UI/material-dashboard-pro-react/src/components/CustomInput/CustomInput.jsx"

import extendedFormsStyle from "../../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/views/extendedFormsStyle.jsx";


class NuevaCitaClientes extends Component {

  render() {
    const { classes } = this.props;
    return (
      <>
        <Card style={{maxWidth: 400, marginTop: 20}}>
          <CardHeader color="rose" icon>
            <CardIcon color="rose">
              <Today />
            </CardIcon>
            <h4 className={classes.cardIconTitle}>Nueva cita</h4>
          </CardHeader>
          <CardBody>
            <FormControl className={classes.botonFormulario} fullWidth>
              <Datetime
                style={{flex:1}}
                inputProps={{ placeholder: "Elige día y hora" }}
              />
            </FormControl>

            <CustomInput
              labelText="Nombre del paciente"
              id="nombre"
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "text"
              }}
            />

            <CustomInput
              labelText="Actuación"
              id="actuacion"
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "text"
              }}
            />

            <CustomInput
              labelText="Teléfono"
              id="telefono"
              formControlProps={{
                fullWidth: true
              }}
              inputProps={{
                type: "number"
              }}
            />

          </CardBody>

          <CardFooter  style={{ textAlign: 'center' }}>
            <Button color="primary" type="submit">Enviar</Button>
          </CardFooter>
        </Card>
      </>
    )
  }
}

export default withStyles(extendedFormsStyle)(NuevaCitaClientes)
