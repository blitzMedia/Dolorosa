import React, { Component } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { signOut } from '../../../../store/actions/authActions'

import CalClientes from '../../../calendar/CalClientes'


// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
import { cardTitle } from "../../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react.jsx";


const styles = {
  cardTitle,
  pageSubcategoriesTitle: {
    color: "#3C4858",
    textDecoration: "none",
    textAlign: "center"
  },
  cardCategory: {
    margin: "0",
    color: "#999999"
  }
};

class Citas extends Component {
  render() {
    const { auth } = this.props
    if (!auth.uid) return <Redirect to='/' />

    return (
      <CalClientes />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut()),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withStyles(styles)
)(Citas)
