import React from "react"
import { compose } from 'redux'
import { connect } from 'react-redux'

import Button from '@material-ui/core/Button'

// Core components
import GridContainer from "../../UI/material-dashboard-pro-react/src/components/Grid/GridContainer.jsx";
import GridItem from "../../UI/material-dashboard-pro-react/src/components/Grid/GridItem.jsx";

import { makeMeAdmin } from '../../store/actions/authActions'

class SuperSecret extends React.Component {

  makeMeAdmin = () => {
    console.log('making admin')
    const {email} = this.props.auth

    this.props.makeMeAdmin(email)
  }

  render() {
    console.warn('PROPS', this.props)

    return (
      <>
        <GridContainer className="General" style={{marginTop:'50px'}}>
          <GridItem xs={12} sm={12} md={10}>
            <Button variant="contained" color="primary" onClick={this.makeMeAdmin}>Make me admin!</Button>
          </GridItem>
        </GridContainer>
      </>
    )
  }
}

const mapStateToProps = (state) => {
  console.warn('STATE', state)
  return {
    auth: state.firebase.auth
  }
}

const mapDispatchToProps = (dispatch)=> {
  return {
    makeMeAdmin: (email) => dispatch(makeMeAdmin(email))
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(SuperSecret)
