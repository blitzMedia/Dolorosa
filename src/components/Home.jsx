import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import { signOut } from '../store/actions/authActions'

import FlexContainer from 'react-styled-flexbox'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActionArea from '@material-ui/core/CardActionArea'

import { MiniImg, LogBox } from '../things/StyledComponents'

import Cliente from '../graphics/undraw_cliente.svg'
import Especialista from '../graphics/undraw_especialista.svg'

class Home extends React.Component {
  componentWillMount() { this.props.signOut() }
  componentDidMount() { this.props.signOut() }

  render() {
    return (
      <FlexContainer className="full" itemsCenter justifyCenter directionColumn>
        <h1>Bienvenid@ a Indolora</h1>
        <Card>
          <CardContent>
            <FlexContainer itemsCenter justifySpaceBetween>
              <CardActionArea component={Link} to="/login">
                <LogBox>
                    <FlexContainer itemsCenter justifyFlexEnd directionColumn>
                      <MiniImg src={Cliente} alt="Cliente"/>
                      <h3>Clientes</h3>
                    </FlexContainer>
                </LogBox>
              </CardActionArea>

              <CardActionArea component={Link} to="/signin" style={{ borderLeft: '1px solid gainsboro' }}>
                <LogBox>
                  <FlexContainer itemsCenter justifyFlexEnd directionColumn>
                    <MiniImg src={Especialista} alt="Especialista"/>
                    <h3>Especialistas</h3>
                  </FlexContainer>
                </LogBox>
              </CardActionArea>
            </FlexContainer>
          </CardContent>
        </Card>
      </FlexContainer>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: () => dispatch(signOut()),
  }
}

export default connect(null, mapDispatchToProps)(Home)
