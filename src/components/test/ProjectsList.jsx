import React from 'react'
import ProjectSummary from './ProjectSummary'
import List from '@material-ui/core/List';

const ProjectsList = ({projects}) => {
  return (
    <List>
      { projects && projects.map(project => <ProjectSummary key={project.id} project={project} />) }
    </List>
  )
}

export default ProjectsList
