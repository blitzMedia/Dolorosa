import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchCal } from '../../store/actions/calActions'

import CalSummary from './CalSummary'
import List from '@material-ui/core/List'

class Cal extends Component {

  render() {
    const { events } = this.props.cal || null
    return (
      <>
        <List>
          {
            events && events.map(evento => <CalSummary key={evento.id} evento={evento} />)
          }
        </List>
      </>
    )
  }

}

const mapStateToProps = (state) => {
  return {
    role: state.firebase.profile.role,
    cal: state.cal
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCal: () => dispatch(fetchCal())
    //fetchCal: () => dispatch(fetchCal())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cal)
