import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'
import moment from 'moment'
import 'moment/locale/es'
moment.locale('es')

const CalSummary = ({evento}) => {
  console.log(evento)
  const startTime = moment(evento.start).format('LLLL') || ''
  const endTime = moment(evento.end).format('LLLL') || ''

  return (
    <>
      <ListItem>
        <ListItemText
          primary={`${evento.createdBy} - ${evento.title}`}
          secondary={`${startTime} - ${endTime}`} />
      </ListItem>
      <Divider />
    </>
  )
}

export default CalSummary
