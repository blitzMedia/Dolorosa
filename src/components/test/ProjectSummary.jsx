import React from 'react'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider'

const ProjectSummary = ({project}) => {
  return (
    <>
      <ListItem>
        <ListItemText primary={project.title} secondary= {project.content} />
      </ListItem>
      <Divider />
    </>
  )
}

export default ProjectSummary
