import React from 'react'
import { connect } from 'react-redux'

import { invitarEspecialista } from '../../../store/actions/authActions'

import FlexContainer from 'react-styled-flexbox'

import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import AccountCircle from '@material-ui/icons/AccountCircle'

import reptil from'../../../graphics/contemplative-reptile.jpg'

class InvitarEspecialista extends React.Component {
  state = { email: '' }

  handleSubmit = e => {
    e.preventDefault()
    this.props.invitarEspecialista(this.state.email)
  }

  render() {
    const { role, auth } = this.props

    // Si no es usuario ni admin, nada
    if(!auth.uid && role !== 'admin') return null
    else return (
      <FlexContainer className="full" itemsCenter justifyCenter>
        <form onSubmit={this.handleSubmit} >
          <Card style={{maxWidth: 345}}>
              <CardMedia
                style={{height: 140}}
                image={reptil}
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  Nuevo especialista
                </Typography>
                <TextField
                  id="input-with-icon-textfield"
                  type="email"
                  required
                  label="Email"
                  onChange={e => { this.setState({email: e.target.value}) }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <AccountCircle />
                      </InputAdornment>
                    ),
                  }}
                />
              </CardContent>
            <CardActions >
              <Button type="submit" size="small" color="primary">
                Invitar
              </Button>
            </CardActions>
          </Card>
        </form>
      </FlexContainer>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    role: state.auth.role
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    invitarEspecialista: (mail) => dispatch(invitarEspecialista(mail))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(InvitarEspecialista)
