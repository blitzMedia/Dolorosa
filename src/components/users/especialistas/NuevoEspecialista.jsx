import React from 'react'
import { Redirect } from 'react-router-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'

import { signInWithGAPI } from '../../../store/actions/authActions'
import { setEspecialista } from '../../../store/actions/calActions'

import FlexContainer from 'react-styled-flexbox'

import GoogleButton from 'react-google-button'

import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'


// @material-ui/core components
import withStyles from '@material-ui/core/styles/withStyles'
import InputAdornment from '@material-ui/core/InputAdornment'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import FormControl from '@material-ui/core/FormControl'
import Email from '@material-ui/icons/Email'
import loginPageStyle from '../../../UI/material-dashboard-pro-react/src/assets/jss/material-dashboard-pro-react/views/loginPageStyle.jsx'

import fans from '../../../graphics/undraw_fans.svg'
import joy from '../../../graphics/undraw_joyride.svg'
import wait from '../../../graphics/undraw_waiting.svg'
import sergio from '../../../graphics/SergioConSombrero.png'
import { MiniImg } from '../../../things/StyledComponents'

class NuevoEspecialista extends React.Component {
  state = {
    email: '',
    invitado: true
  }
  componentDidMount() {
    if(window.gapi.auth2.getAuthInstance().isSignedIn.get()) {
      const token = window.gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token
      this.props.signInWithGAPI(token, true)
    }
  }

  gSignIn = () => {
    if(!window.gapi.auth2.getAuthInstance().isSignedIn.get()) {
      window.gapi.auth2.getAuthInstance().signIn()
    }
  }

  esInvitado = (email) => {
    const invitados = this.props.invitados.map(inv => inv.email)
    return invitados.includes(email)
  }

  alCambiar = (e) => {
    this.setState({ [e.target.id]: e.target.value })
  }

  alEnviar = (e) => {
    e.preventDefault()
    // Initial state
    if(this.esInvitado(this.state.email)) {
      this.gSignIn()
    } else {
      // Otherwise, we set it to false
      this.setState({invitado: false})
    }
  }

  render() {
    const { props } = this
    if (!props.invitados) return null


    const { profile, classes } = props, { email } = this.state

    const ourGirls = email === 'silviafhueso@gmail.com' || email === 'carolina.ordonez@indolora.es',
          img = ourGirls ? sergio : fans

    const weAreAuthorized = (props.role === 'especialista' || props.role === 'admin') ? true : false

    let fase1 = this.state.invitado
        ? (
            <>
              <div className={ ourGirls ? 'simpleEntrance dealWithIt' : null }>
                <MiniImg src={img} alt="Nuevo especialista" />
              </div>

              <Typography variant="h5" component="h2">¡Bienvenid@!</Typography>
              {
                profile.isEmpty
                ? (
                  <form onSubmit={this.alEnviar}>
                    <br/>
                    <p>
                      Has sido invitado a la aplicación de indolora.
                      <br/>
                      Introduce el email con el que has sido invitad@ y haz click en el botón para crear tu cuenta.
                    </p>
                    <br/>
                    <FlexContainer justifyCenter directionColumn>
                      <div>
                        <FormControl fullWidth >
                          <InputLabel htmlFor="email">Email</InputLabel>
                          <Input
                            id="email"
                            type="email"
                            onChange={this.alCambiar}
                            // eslint-disable-next-line
                            inputProps={{ pattern: "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" }}
                            inputRef={el => (this.email = el)}
                            fullWidth
                            endAdornment={
                              <InputAdornment position="end">
                                <Email className={classes.inputAdornmentIcon} />
                              </InputAdornment>
                            }
                          />
                        </FormControl>
                      </div>
                      <Button color="primary" size="large" type="submit">
                        ¡Vamos!
                      </Button>
                    </FlexContainer>
                  </form>
                ) : (
                  <p>Cargando siguiente fase...</p>
                )
              }
            </>
          )
          : (
            <>
              <MiniImg src={wait} alt="Nuevo especialista"/>
              <Typography variant="h5" component="h2">Entendemos tu entusiasmo, {email}, <br /> pero espera a que te invitemos!</Typography>
            </>
          )


    const fase2 = (
      <>
        <MiniImg src={joy} alt="Nuevo especialista"/>
        <Typography variant="h5" component="h2">¡Enhorabuena, {props.profile.displayName}!</Typography>
        <br/>
        <p>Hemos creado tu cuenta. Ahora sólo falta crear tus calendarios y podrás empezar a usar Indolora.</p>
        <br/>
        <GoogleButton label="¡Vamos allá!" onClick={props.setEspecialista} />
      </>
    )

    return (
      <FlexContainer className="full text-center" itemsCenter justifyCenter directionColumn>
        <Card style={{ maxWidth: 400 }}>
          <CardContent style={{padding: 32}}>
            <FlexContainer itemsCenter justifyCenter directionColumn>
              {
                props.profile.calendars && <Redirect to="/especialistas" />
              }
              {
                weAreAuthorized ? (fase2) : (fase1)
              }
            </FlexContainer>
          </CardContent>
        </Card>
      </FlexContainer>
    )
  }
}

const mapStateToProps = (state) => {
  console.log(state)
  return {
    auth: state.firebase.auth,
    invitados: state.firestore.ordered['invitados'],
    profile: state.firebase.profile,
    role: state.auth.role,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signInWithGAPI: (token, espi) => dispatch(signInWithGAPI(token, espi)),
    setEspecialista: () => dispatch(setEspecialista()),
  }
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  firestoreConnect([{ collection: 'invitados' }]),
  withStyles(loginPageStyle)
)(NuevoEspecialista)
