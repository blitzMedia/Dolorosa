export const setProgressBar = (isOpen) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({ type: 'SET_PROGRESS_BAR', isOpen: isOpen })
  }
}

export const setAlert = (action) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({ type: 'SET_ALERT', alert: action })
  }
}

export const unsetAlert = (action) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({ type: 'UNSET_ALERT', alert: null })
  }
}
