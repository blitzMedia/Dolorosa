import moment from 'moment'
import { setProgressBar, setAlert } from '../../store/actions/otherActions'
//import { newNot } from './notificationActions'
const gapi = window.gapi
//const theTimeZone = 'Europe/Madrid'

// 1: Cita a la Base de Datos
export const citaToAdminDB = (cita, action) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    console.log(cita, action)

    dispatch(setProgressBar(true))

    const firebase = getFirebase()
    const firestore = getFirestore()
    const functions = firebase.functions()
   // const UID = getState().firebase.auth.uid

    // Si estamos creando la cita
    if(action === 'create') {

          /*cita.especialistas.forEach(async esp => {
            const req = await firestore.collection('users').get()

            await req.docs.map(doc => {
              const user = doc.data()
              if(user.email !== esp.email) return null

              const { FCMToken } = user
              sendNot(FCMToken, cita)
            })
            return null
          }) */

      try {
        const request = await firestore.collection('citas').add(cita)
        await firestore.collection('citas').doc(request.id).update({id: request.id, nueva: true})

        cita.id = request.id

        const nuevaCitaParaTodos = functions.httpsCallable('nuevaCitaParaTodos')
        await nuevaCitaParaTodos(cita)

        //await firestore.collection('notificaciones').add(newNot(cita))


        dispatch({ type: 'NUEVA_CITA_TO_ADMIN_DB_SUCCESS' })

        dispatch(setAlert({
          text: 'Su nueva cita está lista. Pronto recibirá un email de validación',
          status: 'success'
        }))

      } catch(err) {
        dispatch({ type: 'NUEVA_CITA_TO_ADMIN_DB_ERROR' }, err)
      }
    } else if (action === 'update') {
      try {
        console.warn('SUPERCITA', cita)
        await firestore.collection('citas').doc(cita.id).set(cita)

        dispatch({ type: 'UPDATE_CITA_TO_ADMIN_DB_SUCCESS' })

        dispatch(setAlert({
          text: 'La cita  ha sido modificada!',
          status: 'success'
        }))
      } catch(err) {
        console.log(err)
        dispatch({ type: 'UPDATE_CITA_TO_ADMIN_DB_ERROR' }, err)
      }
    }
    // Si estamos validando
    else if (action === 'validate') {
      try {
        const autorizada = {
          ...cita.autorizada,
          porAdmin: !cita.autorizada.porAdmin
        }
        //await firestore.collection('citas').doc(cita.id).set(cita)
        await firestore.collection('citas').doc(cita.id).update({autorizada})

        const citaValidadaPorAdmin = functions.httpsCallable('citaValidadaPorAdmin')
        await citaValidadaPorAdmin(cita)

        dispatch({ type: 'UPDATE_CITA_TO_ADMIN_DB_SUCCESS', cita })

        const text =  autorizada.porAdmin
                    ? 'La cita ha sido validada. El siguiente paso sera parte del cliente'
                    : 'Hemos anulado la cita'

        dispatch(setAlert({
          text,
          status: 'success'
        }))
      } catch(err) {
        console.log(err)
        dispatch({ type: 'UPDATE_CITA_TO_ADMIN_DB_ERROR' }, err)
      }


    } else if (action === 'delete') {
      try {
        await firestore.collection('citas').doc(cita.id).delete()
        dispatch({ type: 'DELETE_CITA_TO_ADMIN_DB_SUCCESS' })
      } catch(err) {
        dispatch({ type: 'DELETE_CITA_TO_ADMIN_DB_ERROR' }, err)
      }
    }

    dispatch(setProgressBar())
  }
}

// 2: Especialista acepta / 4: Cliente acepta
export const aceptarCita = (id, role) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    // Go
    dispatch(setProgressBar(true))

    const state = getState(),
          firestore = getFirestore(),
          firebase = getFirebase(),
          functions = firebase.functions(),
          email = state.firebase.profile.email

    const request = await firestore.collection('citas').doc(id).get()
    const cita = await request.data()

    // Si llega aqui un especialista
    if (role === 'especialista') {
      const slotDetails = [...cita.slotDetails]

      // Ponemos el OK a aquel que esté aceptando
      // HERE BLOCK CALENDAR
      slotDetails.forEach( (slot) => {
        if(slot.especialista === email) {
          slot.ok = true
        }
      })

      // Sustituimos en DB el estado de aceptación
      await firestore.collection('citas').doc(id).update({slotDetails})

      // Mandamos un email al admin
      const citaConfirmadaParaAdmin = functions.httpsCallable('citaConfirmadaParaAdmin')
      await citaConfirmadaParaAdmin({cita, email})

      dispatch(setProgressBar())

      dispatch(setAlert({
        text: 'La cita ha sido aceptada! Pronto recibirás confirmación',
        status: 'success'
      }))

      try {
        dispatch({ type: 'CLOSE_MODAL' })
      } catch(err) {
        dispatch({ type: 'CLOSE_MODAL', err })
      }
    }

    // Si llega aqui un cliente
    if (role === 'cliente') {
      const autorizada = { ...cita.autorizada, porCliente: true }

      // Admin
      const citaConfirmadaPorClienteParaAdmin = functions.httpsCallable('citaConfirmadaPorClienteParaAdmin')
      await citaConfirmadaPorClienteParaAdmin(cita)

      // Cliente
      const citaConfirmadaPorClienteParaEspis = functions.httpsCallable('citaConfirmadaPorClienteParaEspis')
      await citaConfirmadaPorClienteParaEspis(cita)

      if(autorizada.porAdmin && autorizada.porCliente) {
        await firestore.collection('citas-definitivas').add(cita)
      }

      dispatch(setProgressBar())

      dispatch(setAlert({
        text: 'Ha confirmado su cita. Muchas gracias!',
        status: 'success'
      }))
    }
  }
}

export const crearEvento = (cita) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))

    // TODO: Arreglar
    const calendar = 'm1fg7qd3uqajg8234dig1f6oo8@group.calendar.google.com'
    const calObj = await buildCitaDefinitiva(calendar, cita)

    try {
      // Check if cita exists
      await gapi.client.calendar.events.insert(calObj)
      dispatch({ type: 'NEW_CITA_DEFINITIVA_SUCCESS' })
    } catch(err) {
      dispatch({ type: 'NEW_CITA_DEFINITIVA_ERROR', err })
    }

    dispatch(setProgressBar())
  }
}

export const buildCitaDefinitiva = async (calendar, cita) => {
  const descripcion = `Nueva cita con ${cita.cliente.displayName}.
    Dirección: ${cita.cliente.direccion}.
    Nombre del paciente: ${cita.cliente.nombre_paciente}.
    Procedimiento a realizar: ${cita.detalles.procedimiento}.
    Especialista: ${cita.autorizada.especialista}`;

  const event = {
    'summary': `Cita confirmada: ${cita.cliente.displayName} - ${cita.detalles.procedimiento}`,
    //'location': cita.cliente.fullAddress || '',
    'description': descripcion,
    'start': {
      'dateTime': moment(cita.detalles.start).toISOString(),
      'timeZone': 'Europe/Madrid'
    },
    'end': {
      'dateTime': moment(cita.detalles.end).toISOString(),
      'timeZone': 'Europe/Madrid'
    },
    'attendees': [
      {'email': cita.autorizada.especialista},
    ]
  }

  const calObj = {
    'calendarId': calendar,
    'resource': event
  }

  return calObj
}

export const convertCitasToCalendar = (citas) => (
  citas.map(cita => ({
    title: cita.cliente.displayName,
    slotDetails: cita.slotDetails,
    allDay: false,
    start: moment(cita.detalles.start).toDate(),
    end: moment(cita.detalles.end).toDate(),
    backgroundColor: 'red',
    type: 'cita',
    ...cita,
    mix: true
  }))
)

export const buildCita = (cita, profile, UID, auther, id) => ({
  cita,
  id: id || '',
  cliente: profile,
  createdAt: new Date(),
  autorizada: auther || false,
  type: 'cita'
})

export const buildCitaConEspecialista = async (calId, cita) => {
  const descripcion = `Nueva cita con ${cita.cliente.displayName}.
  Dirección: ${cita.cliente.fullAddress}.
  Nombre del paciente: ${cita.cliente.nombre_paciente}.
  Procedimiento a realizar: ${cita.detalles.procedimiento}.`

  const event = {
    //'summary': `${cita.cliente.displayName} - ${cita.detalles.procedimiento}`,
    //'location': cita.cliente.fullAddress || '',
    'description': descripcion,
    'start': {
      'dateTime': moment(cita.detalles.start).toISOString(),
      'timeZone': 'Europe/Madrid'
    },
    'end': {
      'dateTime': moment(cita.detalles.end).toISOString(),
      'timeZone': 'Europe/Madrid'
    },
    /* 'attendees': [
      {'email': cita.cliente.email},
      {'email': cita.especialista.email}
    ] */
  }

  const calObj = {
    'calendarId': calId,
    'resource': event
  }

  console.log(JSON.stringify(event,null,'\t'))
  return calObj
}


/*
const newNot = (cita) => {
  return {
    cliente: cita.cliente.displayName,
    inicio: cita.detalles.start,
    id: cita.id
  }
}

function sendNot(token, cita) {
  const postURL = 'https://fcm.googleapis.com/fcm/send'
  const body = {
    "to" : token,
    "collapse_key" : "type_a",
    "data" : {
      "body" : "First Notification",
      "title": "ALT App Testing",
      "paciente" : cita.detalles.nombre_paciente,
      "key_2" : "Hellowww"
    }
  }
  const headers = {
    "Authorization": "key=AAAAO5h1COE:APA91bHrHeT8luc5UK7_ZcqxpIcOKoyC1-5FXnwM83M3kx2wXwyOsEc41hVnYejVfBaGHT-THQmXPgIijH_wah_KmNf6ZebPaFXfIAd1ZNN27gbqMtnx-FxGNdnu2owEOEsQPhkbiqZ-",
    "Content-Type": "application/json"
  }

  fetch(postURL, {
    method: 'POST',
    body: JSON.stringify(body),
    headers
  }).then(res => {
    res.json()}
    )
    .then(data => {
      // Aqui nuestra respuesta 200
      const response = data
      console.log(response)
    })
} */
