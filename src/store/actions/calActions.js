import { setProgressBar, setAlert } from '../../store/actions/otherActions'
import {
  formatEventsForCalendar,
  listAllEventsById
} from './eventActions'

const gapi = window.gapi
const theTimeZone = 'Europe/Madrid'

// eslint-disable-next-line
String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
//const theCalId = 'Indolora: Disponibilidad'

export const setEspecialista = () => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))
    await dispatch(checkCals())
  }
}

export const checkCals = () => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    // Google

    const req = await gapi.client.calendar.calendarList.list()
    const userCals = req.result.items

    const indoloraCals = userCals.filter(cal => cal.summary.includes('[IND]'))

    // Si no hay ninguno...
    if(!indoloraCals.length) dispatch(createCal(['disponibilidad', 'citas']))
    else if (indoloraCals.length === 1) {
      // Vemos cual falta
      indoloraCals[0].summary.includes('disp')
        ? dispatch(createCal(['citas']))
        : dispatch(createCal(['disponibilidad']))
    }
    else if (indoloraCals.length === 2) dispatch(setCalsInDB(indoloraCals))
  }
}

export const setCalsInDB = (indoloraCals) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore()
    const calendars = {}
    indoloraCals.forEach(cal => {
      if(cal.summary.toLowerCase().includes('citas')) {
        calendars['citas'] = cal.id
      } else {
        calendars['disponibilidad'] = cal.id
      }
    })
    console.log(calendars)

    try {
      const state = getState(), uid = state.firebase.auth.uid
      await firestore.collection('users').doc(uid).update({calendars})
      dispatch({ type: 'THERE_ARE_CALS', indoloraCals })
    } catch(err) {
      console.log('FUCKIN ERRR')
    }
  }
}

export const createCal = (calendars) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    const firestore = getFirestore()
    const UID = getState().firebase.auth.uid

    try {
      calendars.forEach(async (calendar, i) => {
        const newCalObj = {
          summary: `Indolora: ${calendar.capitalize()} [IND]`,
          description: `Calendario de ${calendar}`,
          timeZone: theTimeZone
        }

        const newCal = await window.gapi.client.calendar.calendars.insert(newCalObj)
        const newCalID = newCal.result.id

        var req = {
          "calendarId": newCalID,
          "resource": {
            "role": "reader",
            "scope": {
              "type": "default",
              "value": ""
            }
          }
        }

        await window.gapi.client.calendar.acl.insert(req)


        // We check if its the second iteration
        if( i === (calendars.length - 1) ) {
          dispatch(setProgressBar())

          dispatch(setAlert({
            text: 'Genial! Hemos creado tus calendarios. Lo siguiente: definir tu disponibilidad.',
            status: 'success'
          }))
        }

        // SUBIMOS EL ID A LA DB
        calendar === 'citas'
        ? await firestore.collection('users').doc(UID).update({
            'calendars.citas': newCalID
          })
        : await firestore.collection('users').doc(UID).update({
            'calendars.disponibilidad': newCalID
          })


        // Dispatch to REDUX
        dispatch({ type: 'NEWCAL', newCalID })
      })
    } catch(err) {
      console.log('Algo pasa', err)
    }
  }
}

export const fetchCal = (calId) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))

    const start = async () => {
      await gapi.client.init({
        apiKey: process.env.REACT_APP_GAPI_KEY,
        clientId: process.env.REACT_APP_GAPI_CLIENT_ID,
        discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
        scope: 'https://www.googleapis.com/auth/calendar'
      })

      await gapi.auth2.getAuthInstance()

      const events = await listAllEventsById(calId)

      dispatch({ type: 'CAL_FETCH_SUCCESS', events })
      dispatch(setProgressBar())
    }

    try {
      gapi.load('client', start)
    } catch(err) {
      dispatch({ type: 'CAL_FETCH_ERROR', err })
    }
  }
}

export const fetchAllCals = (clean) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))

    const start = async () => {
      await gapi.client.init({
        apiKey: process.env.REACT_APP_GAPI_KEY,
        clientId: process.env.REACT_APP_GAPI_CLIENT_ID,
        discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
        scope: 'https://www.googleapis.com/auth/calendar'
      })

      await gapi.auth2.getAuthInstance()

      const req = await gapi.client.calendar.calendarList.list()
      const userCals = await req.result.items
      const filteredUserCals = userCals.filter(cal => cal.summary.includes('[IND]'))

      console.log(111, 'CAAAAALS', filteredUserCals)

      filteredUserCals.forEach(async cal => {
        const events = await listAllEventsById(cal.id)
        dispatch({ type: 'CAL_FETCH_SUCCESS', events })
      })

      dispatch(setProgressBar())
    }

    try {
      gapi.load('client', start)
    } catch(err) {
      dispatch({ type: 'CAL_FETCH_ERROR', err })
    }
  }
}

export const checkDispCal = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))

    const start = async () => {
      await gapi.client.init({
        apiKey: process.env.REACT_APP_GAPI_KEY,
        clientId: process.env.REACT_APP_GAPI_CLIENT_ID,
        discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
        scope: 'https://www.googleapis.com/auth/calendar'
      })

      await gapi.auth2.getAuthInstance()

      const req = await gapi.client.calendar.calendarList.list()
      const userCals = req.result.items

      const existsDispCal = userCals.find(cal => cal.summary.toLowercase().includes('disp'))

      if(existsDispCal) {
        const calId = existsDispCal.id
        dispatch({ type: 'THERE_IS_CAL', calId })
        dispatch(fetchCal(calId))
      } else {
        dispatch({ type: 'THERE_IS_NO_CAL' })
      }
    }

    try {
      gapi.load('client', start)
    } catch(err) {
      console.log('CheckDispCal error: ', err)
    }

    dispatch(setProgressBar())
  }
}

export const checkCitasCal = () => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))

    const req = await gapi.client.calendar.calendarList.list()
    const userCals = req.result.items

    const existsDispCal = userCals.find(cal => cal.summary === 'Indolora: Citas')

    if(existsDispCal) {
      const calId = existsDispCal.id
      dispatch({ type: 'THERE_IS_CITAS_CAL', calId })
      console.log(existsDispCal)
      dispatch(fetchCal())
    } else {
      dispatch({ type: 'THERE_IS_NO_CITAS_CAL' })
    }

    dispatch(setProgressBar())
  }
}

export const fetchDisponibilidad = () => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))

    const firestore = getFirestore()
    let eventos = []

    const req = await firestore.collection("users").get()

    const especialistas = await req.docs.map(doc => {
      const user = doc.data()
      if(!user.role === 'especialista') return null
      return user
    })

    const start = async () => {
      await gapi.client.init({
        apiKey: process.env.REACT_APP_GAPI_KEY,
        clientId: process.env.REACT_APP_GAPI_CLIENT_ID,
        discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
        scope: 'https://www.googleapis.com/auth/calendar'
      })

      await gapi.auth2.getAuthInstance()

      const getEvents = (especialistas) => {
        const promises = especialistas.map(async (especialista) => {
          if(especialista !== undefined) {
            return {
              id: "my_id",
              myValue: await gapi.client.calendar.events.list({
                calendarId: especialista.calendarId,
                showDeleted: false,
                singleEvents: true,
                orderBy: 'startTime'
              })
            }
          }
        })
        return Promise.all(promises)
      }

      const superEventos = await getEvents(especialistas)

      console.log(superEventos)

      req.forEach(user => {
        const u = user.data()

        if(u.role === 'especialista' && u.calendarId !== undefined) {

          gapi.client.calendar.events.list({
            calendarId: u.calendarId,
            showDeleted: false,
            singleEvents: true,
            orderBy: 'startTime'
          }).then(events => {
            eventos = [ ...formatEventsForCalendar(events.result.items), ...eventos ]

            /* const endDates = eventos.map(ev => moment(ev.end).toDate())
            const maxDate = new Date(Math.max.apply(null, endDates.map(function(e) {
              return new Date(e);
            })))
            console.log(endDates)
            console.log(maxDate) */

            dispatch('FETCH_ALL_DISP_SUCCESS', eventos)
          }, error => {
            dispatch('FETCH_ALL_DISP_ERROR', error)
          })
        }

      })

    }

    try {
      gapi.load('client', start)
    } catch(err) {
      console.log('CheckDispCal error: ', err)
    }
  }
}

export const fetchAllDisponibilidad = () => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))
    const firestore = getFirestore()

    //HERE
    const getEm = await firestore.get({collection: 'users'})

    const especialistas = await getEm.docs

    try {
      especialistas.forEach(async (esp, i) => {
        const data = esp.data()
        // Si no hay calendario sal
        if(!data.calendars || !data.calendars.disponibilidad) return

        // Busca eventos
        const disponibilidades = await listAllEventsById(data.calendars.disponibilidad)

        dispatch({ type: 'FETCH_DISP_ADD', disponibilidades })
      })
    } catch(err) {
      dispatch({ type: 'FETCH_DISP_ERROR' })
    }

    dispatch(setProgressBar())
  }
}

export const fetchMyDisponibilidad = () => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))

    const uid = await getFirebase().auth().currentUser.uid

    // Get the user by UID
    try {
      var user = await getFirestore().collection('users').doc(uid).get()
      var { disponibilidad } = user.data().calendars
    } catch(err) {
      console.log('USER GET ERROR', err)
    }

    // Get the events
    try {
      var events = await listAllEventsById(disponibilidad)
    } catch(err) {
      console.log('EVENTS GET ERROR', err)
    }

    dispatch({ type: 'FETCH_ALL_DISP_ONCE_SUCCESS', events })

    dispatch(setProgressBar())
  }
}

/*

Helpers

*/

export const makeCalendarPublic = async (id) => {
  console.log(id)
  var req = {
    "calendarId": id,
    "resource": {
      "role": "reader",
      "scope": {
        "type": "default",
        "value": ""
      }
    }
  }

  await window.gapi.client.calendar.acl.insert(req)

  console.log('Calendar is public')
  return
}

// eslint-disable-next-line
Date.prototype.getMonday = function() {
  var d = new Date(this.getTime())
  //console.log(d)
  var diff = d.getDate() - d.getDay() + 1
  if (d.getDay() === 0)
      diff -= 7
  //diff += 7; // ugly hack to get next monday instead of current one
  const monday = new Date(d.setDate(diff))
  return monday
}

// eslint-disable-next-line
Date.prototype.getFriday = function() {
  var d = this.getMonday()
  return new Date(d.setDate(d.getDate() + 4));
}
