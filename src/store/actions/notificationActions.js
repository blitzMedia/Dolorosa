//import { setProgressBar, setAlert } from '../../store/actions/otherActions'
import moment from 'moment'

export const notifyCita = (cita) => {
  console.log(1234567890, cita)

  const notification =  {
    cliente: cita.cliente.displayName,
    inicio: cita.detalles.start
  }

  //dispatch(newNot(notification))
}

export const newNot = (cita) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    const not =  {
      cliente: cita.cliente.displayName,
      inicio: cita.detalles.start
    }

    await getFirestore().collection('notificaciones').add(not)

    dispatch({ type: 'NOT_SUCCESS', not })
  }
}
