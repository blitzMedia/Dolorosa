import { setProgressBar, setAlert } from '../../store/actions/otherActions'
const gapi = window.gapi

// Para clientes
export const signInWithEmailAndPassword = (creds) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase()

    firebase.auth().signInWithEmailAndPassword(
      creds.email,
      creds.password
    ).then(() => {
      dispatch({ type: 'LOGIN_SUCCESS' })
    }).catch((err) => {
      dispatch({ type: 'LOGIN_ERROR' }, err)
    })
  }
}

// Para el resto
export const signInWithGAPI = (token, espi) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase()
    const firestore = getFirestore()
    const functions = firebase.functions()

    const gMail = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile().getEmail()


    // Array vacío de usuarios que rellenaremos en foreach
    let usuariosInvitados = []

    // Buscamos los invitados
    const querySnapshot = await firestore.collection('invitados').get()
    querySnapshot.forEach((doc) => {
      const { email } = doc.data()
      // Los añadimos uno a uno
      usuariosInvitados = [...usuariosInvitados, email]
    })

    // Chequeamos para ver si está en el array
    if( !usuariosInvitados.includes(gMail) ) {
      const gAuth = gapi.auth2.getAuthInstance()

      try {
        await gAuth.signOut()
        gAuth.disconnect()
        getFirebase().auth().signOut()

        dispatch({ type: 'GET_OUTTA_HERE' })
      } catch(err) {
        console.log(err)
      }
      //history.push('/')
    }

    // El usuario sí que está invitado
    else {
      // Getting firebase user
      const credential = firebase.auth.GoogleAuthProvider.credential(token),
      signIn = await firebase.auth().signInAndRetrieveDataWithCredential(credential),
      user = signIn.user

      // Checking firestore and creating user
      const userInDb = await firestore.collection('users').doc(user.uid).get()

      if (userInDb.exists) { console.log('User exists: ', userInDb.data()) }
      else {
        console.log('User doesnt exist')
        const initials = Array.prototype.map.call(user.displayName.split(" "), function(x){ return x.substring(0,1).toUpperCase();}).join('').toString(),
            createdAt = new Date()

        await firestore.collection('users').doc(user.uid).set({
          displayName: user.displayName,
          photoURL: user.photoURL,
          initials,
          email: user.email,
          createdAt,
        })
      }

      // Si es especialista pero no tiene el rol bien puesto
      if(espi === true && getState().auth.role !== 'especialista') {

        let theNewUser = userInDb.data()
        const tokenData = {
          token,
          email: theNewUser.email,
          role: 'especialista'
        }
        const setMyRole = functions.httpsCallable('setRole')

        try {
          await setMyRole(tokenData)

          await dispatch(setAlert({
            title: `Enhorabuena!`,
            text: '¡Tu usuario ha sido creado con éxito!',
            status: 'success'
          }))
        } catch(err) { console.log(err) }

        try {
          const { role } = getState().auth
          if (!role === 'especialista')
          // Uff... hacky
          window.location.replace(`/especialistas/nuevo-especialista`)
        } catch(err) { console.log(err) }
      }

      dispatch({ type: 'GAPI_SIGNIN_SUCCESS' })
    }
  }
}

export const signOut = (creds) => {
  return async (dispatch, getState, { getFirebase}) => {

    // Disconnect from Google first
    const gAuth = gapi.auth2.getAuthInstance()

    try {
      await getFirebase().auth().signOut()
      await gAuth.signOut()
      gAuth.disconnect()

      dispatch({ type: 'SIGNOUT_SUCCESS' })
    } catch(err) {
      console.log(err)
    }

  }
}

export const adminSignUp = (newUser) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase()
    const firestore = getFirestore()

    firebase.auth().createUserWithEmailAndPassword(
      newUser.email,
      newUser.password
    ).then((resp) => {
      const createdAt = new Date()
      return firestore.collection('users').doc(resp.user.uid).set({
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        initials: newUser.firstName[0] + newUser.lastName.match(/\b\w/g).join(''),
        email: newUser.email,
        type: newUser.type,
        password: newUser.password,
        createdAt: createdAt.toString()
      })
    }).then(() => {
      dispatch({ type: 'ADMIN_SIGNUP_SUCCESS' })
    }).catch((err) => {
      dispatch({ type: 'ADMIN_SIGNUP_ERROR', err })
    })
  }
}

export const clienteSignUp = (creds) => {
  const { email, tel, password, lugar: { name, address, suggestion } } = creds

  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))
    const firebase = getFirebase()
    const firestore = getFirestore()
    const functions = firebase.functions()

    try {

      const result = await firebase.auth().createUserWithEmailAndPassword(
        email,
        password
      )

      console.log(result.user.uid)
      const createdAt = new Date()

      await firestore.collection('users').doc(result.user.uid).set({
        displayName: name,
        address,
        email,
        tel,
        createdAt: createdAt.toString(),
        suggestion
      })

      const request = await firestore.collection('users').doc(result.user.uid).get()
      if (request.exists) { console.log('User exists: ', request.data()); }
      let userInDb = request.data()

      try {
        await firebase.auth().signInWithEmailAndPassword(
          userInDb.email,
          password
        )
        dispatch({ type: 'LOGIN_SUCCESS' })
      } catch(err) {
        dispatch({ type: 'LOGIN_ERROR' }, err)
      }

      // Woooh
      const token = await firebase.auth().currentUser.getIdToken(true)

      const tokenData = {
        token,
        email: userInDb.email,
        role: 'cliente'
      }
      const setMyRole = functions.httpsCallable('setRole')
      await setMyRole(tokenData)
      dispatch(setProgressBar())

      await dispatch(setAlert({
        title: `Enhorabuena!`,
        text: '¡Tu usuario ha sido creado con éxito!',
        status: 'success'
      }))

      // Uff... hacky
      setTimeout(() => { window.location.reload() }, 2000)

      dispatch({ type: 'CLIENTE_SIGNUP_SUCCESS' })
    } catch(err) {
      dispatch({ type: 'CLIENTE_SIGNUP_ERROR', err })
    }
  }
}

export const makeMeAdmin = (email) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase()
    const functions = firebase.functions()

    console.log(email, functions)

    const makeMeAdmin = functions.httpsCallable('makeMeAdmin');
    makeMeAdmin({ email }).then(result => {
      console.log(result);
    })
  }
}

export const invitarEspecialista = email => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))
    const firebase = getFirebase()
    const firestore = getFirestore()
    const functions = firebase.functions()
    let usuariosInvitados = []

    const querySnapshot = await firestore.collection('invitados').get()
    querySnapshot.forEach((doc) => {
      const { email } = doc.data()
      // Los añadimos uno a uno
      usuariosInvitados = [...usuariosInvitados, email]
    })

    if(usuariosInvitados.includes(email)) {
      dispatch(setAlert({
        text: `${email} ya ha sido invitado. No hace falta hacerlo dos veces!`,
        status: 'success'
      }))
      return
    }

    try {
      await firestore.collection('invitados').add({email})
      dispatch(setAlert({
        text: `${email} ha sido invitado a la plataforma. Pronto recibirá un mail con instrucciones.
        Una vez se haya registrado, recibirás un email de confirmación.`,
        status: 'success'
      }))
    } catch(err) {
      dispatch(setAlert({
        text: err,
        status: 'error'
      }))
    }

    try {
      const invitarEspecialista = functions.httpsCallable('invitarEspecialista');
      const result = await invitarEspecialista({ email })
      console.log(result)
    } catch(err) {
      dispatch(setAlert({
        text: err,
        status: 'error'
      }))
    }

    dispatch(setProgressBar())
  }
}
