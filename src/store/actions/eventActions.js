import moment from 'moment'
import { setProgressBar } from '../../store/actions/otherActions'
const gapi = window.gapi
const theTimeZone = 'Europe/Madrid'

const buildEvent = async (calId, slotinfo) => {
  console.log(slotinfo)
  const { start, end } = slotinfo

  const eventObj = {
    'end': {
        'dateTime': end.toISOString(),//end,
        'timeZone': theTimeZone
    },
    'start': {
        'dateTime': start.toISOString(),//start,
        'timeZone': theTimeZone
    },
    'summary': 'Disponibilidad',
    'type': 'disponibilidad'
  }

  const calObj = {
    'calendarId': calId,
    'resource': eventObj
  }

  return calObj
}

export const formatEventsForCalendar = (events = [], type = 'undefined') => {
  return events.map(ev => {
    const title = type.toLowerCase() === 'disponibilidad'
                  ? ev.creator.email
                  : 'Disponibilidad'

    // Creamos un objeto con detalles o nada
    const slotDetails = ['disponibilidad', 'clientes'].includes(type.toLowerCase())
                    ? [{
                      'eventId': ev.id,
                      'especialista': ev.creator.email,
                      'ok': false
                    }] : ['']

    return {
      createdBy: ev.creator.email,
      creatorEmail: ev.creator.email,
      summary: ev.summary,
      title,
      allDay: false,
      start: moment(ev.start.dateTime).toDate(),
      end: moment(ev.end.dateTime).toDate(),
      htmlLink: ev.htmlLink,
      id: ev.id,
      cal: ev.organizer.displayName,
      iCalUID: ev.iCalUID,
      type: type,
      slotDetails
    }
  })
}

export const mapEvents = (events) => {
  return events.map(ev => ({
    createdBy: ev.creator.displayName,
    creatorEmail: ev.creator.email,
    mix: false,
    title: ev.summary,
    allDay: false,
    start: moment(ev.start.dateTime).toDate(),
    end: moment(ev.end.dateTime).toDate(),
    htmlLink: ev.htmlLink,
    id: ev.id,
    cal: ev.organizer.displayName,
    iCalUID: ev.iCalUID
  }))
}

// ACTIONS OVER EVENTS
export const newEvent = (slotinfo) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))
    const calId = getState().firebase.profile.calendars.disponibilidad
    const calObj = await buildEvent(calId, slotinfo)

    try {
      var requestNewEvent = await gapi.client.calendar.events.insert(calObj)
      var newEvent = requestNewEvent.result
      dispatch({ type: 'NEW_EVENT_SUCCESS', newEvent })
    } catch(err) {
      dispatch({ type: 'NEW_EVENT_ERROR', err })
    }

    dispatch(setProgressBar())
  }
}

export const updateEvent = (slotinfo) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    // Si está bloqueado, pues nos vamos
    if(slotinfo.summary.includes('lock') || slotinfo.summary.includes('lock')) return

    dispatch(setProgressBar(true))

    const calId = getState().firebase.profile.calendars.disponibilidad

    const calObj = await buildEvent(calId, slotinfo)

    let evId
    slotinfo.event
      ? evId = slotinfo.event.id
      : evId = slotinfo.id

    try {
      const requestNewEvent = await gapi.client.calendar.events.patch({
          'eventId': evId,
          ...calObj
      })
      const newEvent = requestNewEvent.result
      dispatch({ type: 'UPDATE_EVENT_SUCCESS', newEvent  })
    } catch(err) {
      dispatch({ type: 'UPDATE_EVENT_ERROR', err })
    }

    dispatch(setProgressBar())
  }
}

export const deleteEvent = (ev) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch(setProgressBar(true))
    const calendarId = getState().firebase.profile.calendars.disponibilidad
    try {
      await gapi.client.calendar.events.delete({
        calendarId,
        eventId: ev.id
      })
      dispatch({ type: 'DELETE_EVENT_SUCCESS', id: ev.id })
    } catch(err) {
      dispatch({ type: 'DELETE_EVENT_ERROR' })
    }

    dispatch(setProgressBar())
  }
}

export const listAllEventsById = async (id, timeMin, timeMax) => {
  try {
    const events = await window.gapi.client.calendar.events.list({
      calendarId: id,
      showDeleted: false,
      singleEvents: true,
      orderBy: 'startTime',
      //timeMin: timeMin.toISOString() || '',
      //timeMax: timeMax.toISOString() || '',
    })
    return events.result.items
  } catch(err) {
    return err
  }
}

export const checkForOverlaps = events => {

  let formatted = formatEventsForCalendar(events, 'clientes')
  let noEmptyEvents = formatted.filter(ev => ev.start !== undefined)
  let noLockedStatus = noEmptyEvents.filter(ev => !ev.title.toLowerCase().includes('lock'))
  let sorted = noLockedStatus.sort((previous, current) => {
    // get the start date from previous and current
    var previousTime = previous.start.getTime();
    var currentTime = current.start.getTime();

    // if the previous is earlier than the current
    if (previousTime < currentTime) return -1

    // if the previous time is the same as the current time
    if (previousTime === currentTime) return 0

    // if the previous time is later than the current time
    return 1
  })

  // Reduce it
  const overlaps = sorted.reduce((result, current, idx, arr) => {
    // get the previous range
    if (idx === 0) return result
    var previous = arr[idx-1]

    // check for any overlap
    var previousEnd = previous.end.getTime()
    var currentStart = current.start.getTime()
    var overlap = (previousEnd >= currentStart)

    // store the result
    if (overlap) {
      // Compare the mother fuckers
      const fullStart = previous.start <= current.start
                        ? moment(previous.start).toDate()
                        : moment(current.start).toDate()
      const fullEnd = previous.end >= current.end
                        ? moment(previous.end).toDate()
                        : moment(current.end).toDate()

      // La magia final
      const slotDetails = previous.createdBy === current.createdBy
                          // Mismo especialista ?
                          ? [previous.slotDetails]
                          // Dos diferentes?
                          :  [...previous.slotDetails, ...current.slotDetails]

      const newEvent = {
        ...current,
        title: 'Disponibilidad',
        createdBy: 'several',
        creatorEmail: 'several',
        iCalUID: 'several',
        start: fullStart,
        end: fullEnd,
        slotDetails
      }

      result.newEvents.push(newEvent)

      const ids = [current.id, previous.id]
      result.overlapped = [...result.overlapped, ...ids]
    }

    return result

     // seed the reduce
  }, {overlapped: [], newEvents: []})

  // Magia! filtramos por ID y luego concatenamos los nuevos resultados
  return  sorted
          // Que no esten los IDs
          .filter(ev => !overlaps.overlapped.includes(ev.id))
          .concat(overlaps.newEvents)
}

export const bloquearDisponibilidad = (id) => {
  return async (dispatch, getState, { getFirebase, getFirestore }) => {
    console.log(id)
    dispatch(setProgressBar(true))

    const calId = getState().firebase.profile.calendars.disponibilidad

    const requestDispEvent = await gapi.client.calendar.events.get({
      'calendarId': calId,
      'eventId': id
    })
    const dispEvent = requestDispEvent.result

    const newEventPatch = await gapi.client.calendar.events.patch({
      'calendarId': calId,
      'eventId': id,
      'summary': `Disponibilidad - lock`
    })
    const newEvent = newEventPatch.result

    console.log(dispEvent, newEvent)
  }
}
