const initState = { events: [], disponibilidades: [] }
let disponibilidades = []

const calReducer = (state = initState, action) => {
  switch(action.type) {
    case 'NEWCAL_SUCCESS':
      console.log('NUEVO, guay', action)
      return {
        ...state
      }
    case 'NEWCAL_ERROR':
      console.log('NUEVO, no', action)
      return {
        ...state
      }

    case 'THERE_IS_CAL':
    console.log('Hay calendario', action)
      return {
        ...state,
        calDoesntExist: false,
        dispCalId: action.calId
      }

    case 'THERE_IS_NO_CAL':
      console.log('Hay calendario', action)
        return {
          ...state,
          calDoesntExist: true,
        }

    case 'THERE_IS_CITAS_CAL':
      console.log('Hay calendario', action)
      return {
        ...state,
        calDoesntExist: false,
        citasCalId: action.calId
      }

    case 'THERE_IS_NO_CITAS_CAL':
      console.log('Hay calendario', action)
        return {
          ...state,
          calDoesntExist: true,
        }

    case 'CAL_FETCH_SUCCESS':
      console.log('Llega cal!:', action)
      return {
        ...state,
        events: action.events
      }

    case 'CAL_FETCH_ERROR':
    console.log('Error de fetchCal!')
    return {
      ...state,
      calError: action.err.message
    }

    case 'NEW_EVENT_SUCCESS':
      console.log('Nuevo evento!:', action)
      return {
        ...state,
        disponibilidades: [
          ...state.disponibilidades,
          action.newEvent
        ]
      }

    case 'NEW_EVENT_ERROR':
    console.log('No se pudo!')
    return {
      ...state,
      calError: action.err.message
    }

    case 'UPDATE_EVENT_SUCCESS':
      disponibilidades = state.disponibilidades.map(disp => disp.id === action.newEvent.id ? action.newEvent : disp)
      return {
        ...state,
        disponibilidades
      }

    case 'UPDATE_EVENT_ERROR':
    console.log('Nada, macho!')
    return {
      ...state,
      calError: action.err.message
    }

    case 'DELETE_EVENT_SUCCESS':
      disponibilidades = state.disponibilidades.filter(disp => disp.id !== action.id)
      return {
        ...state,
        disponibilidades
      }

    case 'DELETE_EVENT_ERROR':
    console.log('Nada, macho!')
    return {
      ...state,
      calError: action.err.message
    }

    case 'FETCH_DISP_ADD':
      return {
        ...state,
        disponibilidades: [
          // Las que ya hay
          ...state.disponibilidades,
          // Las nuevas
          ...action.disponibilidades
        ]
      }

    case 'FETCH_DISP_DESTROY':
      console.log(999, action)
      return {
        ...state,
        disponibilidades: [...action.events]
      }

    case 'FETCH_ALL_DISP_ERROR':
    console.log('Nada, macho!')
    return {
      ...state,
      dispError: action.err.message
    }

    default:
      return state
  }
}

export default calReducer
