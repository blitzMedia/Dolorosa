const initState = {}

const notReducer = (state = initState, action) => {
  switch (action.type) {
    case 'NOT_SUCCESS':
      console.warn('Notification sent')
      return {
        ...state
      }

    case 'NOT_ERROR':
      console.warn('NOTIFICATION ERROR', action.err)
      return {
        ...state
      }

    default: return state
  }
}

export default notReducer
