import authReducer from './authReducer'
import projectReducer from './projectReducer'
import citaReducer from './citaReducer'
import usersReducer from './usersReducer'
import calReducer from './calReducer'
import notificationReducer from './notificationReducer'
import UIReducer from './UIReducer'
import { firestoreReducer } from 'redux-firestore'
import { firebaseReducer } from 'react-redux-firebase'
import { combineReducers } from 'redux'

const appReducer = combineReducers({
  auth: authReducer,
  project: projectReducer,
  citas: citaReducer,
  users: usersReducer,
  cal: calReducer,
  UI: UIReducer,
  notifications: notificationReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer
})

const rootReducer = (state, action) => {
  if (action.type === 'SIGNOUT_SUCCESS') state = undefined

  return appReducer(state, action)
}

export default rootReducer
