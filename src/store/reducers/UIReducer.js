const initState = {}

const UIReducer = (state = initState, action) => {
  switch(action.type) {
    case 'SET_PROGRESS_BAR':
      return {
        ...state,
        progressBarStatus: action.isOpen
      }

    case 'SET_ALERT':
      console.log('Alert!', action)
      return {
        ...state,
        alert: {
          text: action.alert.text,
          status: action.alert.status
        },
      }

    case 'UNSET_ALERT':
      return {
        ...state,
        alert: null,
      }

    case  'CLOSE_MODAL':
      return {
        ...state,
        closeModal: true
      }

    default:
      return state
  }
}

export default UIReducer
