const initState = {}

const citaReducer = (state = initState, action) => {
  let cita = {}
  switch(action.type) {
    case 'NUEVA_CITA_TO_ADMIN_DB_SUCCESS':
      cita = action.nuevaCita
      console.log('Success create!')
      return {
        ...state
      }
    case 'NUEVA_CITA_TO_ADMIN_DB_ERROR':
      console.log('Error de cita!')
      return {
        ...state,
        citaError: action.err.message
      }

    case 'UPDATE_CITA_TO_ADMIN_DB_SUCCESS':
      cita = action.nuevaCita
      console.log('Success update!', cita)
      return {
        ...state
      }

    case 'UPDATE_CITA_TO_ADMIN_DB_ERROR':
      console.log('Error de update de cita!')
      return {
        ...state,
        citaError: action.err.message
      }

    case 'DELETE_CITA_TO_ADMIN_DB_SUCCESS':
      console.log('Success borrando!', cita)
      return {
        ...state
      }

    case 'DELETE_CITA_TO_ADMIN_DB_ERROR':
      console.log('Error borrando cita!')
      return {
        ...state,
        citaError: action.err.message
      }

    case 'TOGGLE_CITA_CLIENTE_SUCCESS':
      console.log('Success autorizando!', cita)
      return {
        ...state
      }

    case 'TOGGLE_CITA_CLIENTE_ERROR':
      console.log('Error autorizando cita!')
      return {
        ...state,
        citaError: action.err.message
      }

    case 'NUEVO_EVENTO_VALIDADO_CREADO':
      console.log('Fucking success!')
      return {
        ...state
      }

    case 'NUEVO_EVENTO_VALIDADO_ERROR':
      console.log('Error creando evento!')
      return {
        ...state,
        citaError: action.err.message
      }

    case 'NEW_CITA_DEFINITIVA_SUCCESS':
      console.log('Fucking success!')
      return {
        ...state
      }

    case 'NEW_CITA_DEFINITIVA_ERROR':
      console.log('Error creando cita!')
      return {
        ...state,
        citaError: action.err.message
      }

    default:
      return state
  }
}

export default citaReducer
