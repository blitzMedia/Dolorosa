const initState = {
  authError: null
}

const authReducer = (state = initState, action) => {
  switch (action.type) {
    case 'GAPI_SIGNIN_SUCCESS':
      console.warn('We are succesful')
      return { ...state }

    case 'GAPI_SIGNIN_ERROR':
      console.warn(action.err)
      return {
        ...state,
        authError: 'Popup bloqueado. Por favor, permite los popups en tu explorador'
      }

    case 'LOGIN_ERROR':
      console.log('Error de login!')
      return {
        ...state,
        authError: 'Login error'
      }

    case 'LOGIN_SUCCESS':
      console.log('Logueado!')
      return {
        ...state,
        logged: true,
        authError: null
      }

    case 'SIGNOUT_SUCCESS':
      console.log('Estás fuera!')
      return state

    case 'GET_OUTTA_HERE':
      console.log('Get outta here!')
      return {
        ...state,
        youreout: true
      }


    case 'ADMIN_SIGNUP_SUCCESS':
      console.log('Éxito con el signup!')
      return {
        ...state,
        authError: null
      }

    case 'ADMIN_SIGNUP_ERROR':
      console.log('Fracaso con el signup')
      return {
        ...state,
        authError: action.err.message
      }

    case 'GOOGLE_SIGNUP_SUCCESS':
      console.log('Usuario tiene rol')
      return {
        ...state,
        role: action.role
      }

    case 'GOOGLE_SIGNUP_ERROR':
      console.log('Fracaso con el signup')
      return {
        ...state,
        authError: action.err.message
      }

      case 'CLIENTE_SIGNUP_SUCCESS':
      console.log('Usuario!!!')
      return {
        ...state,
      }

    case 'CLIENTE_SIGNUP_ERROR':
      console.log('Pues no...')
      return {
        ...state,
        authError: action.err.message
      }

    case 'CLAIMS_UPDATE':
      console.log('Here comes role', action.payload.claims.role)
      return {
        ...state,
        role: action.payload.claims.role
      }

    default:
      return state
  }
}



export default authReducer
