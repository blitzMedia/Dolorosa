import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from './store/reducers/rootReducer'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { reduxFirestore, getFirestore } from 'redux-firestore'
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase'
import fbConfig from './config/fbConfig'
require('dotenv').config()

/* Init store */

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk.withExtraArgument({ getFirebase, getFirestore })),
    reduxFirestore(fbConfig),
    reactReduxFirebase(fbConfig, {
      useFirestoreForProfile: true,
      userProfile: 'users',
      attachAuthIsReady: true,

      onAuthStateChanged: (user, getFirebase, dispatch) => {
        if (user) {
          user.getIdTokenResult(true).then(idTokenResult => {
            console.warn('user', idTokenResult)
            if(!idTokenResult.claims.role) return
            else {dispatch({ type: 'CLAIMS_UPDATE', payload: idTokenResult })}
          })
            // User is signed in.
        } else {
            //getFirebase().auth.signInAnonymously();
        }
      },
    }),
  //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

  ),
)

/* INIT API */

// Definimos las claves
const apiObj = {
  apiKey: process.env.REACT_APP_GAPI_KEY,
  clientId: process.env.REACT_APP_GAPI_CLIENT_ID,
  discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
  scope: 'https://www.googleapis.com/auth/calendar'
}

// Definimos cómo cargar
function loadGoogleApi() {
  return new Promise(function(resolve, reject) {
    let script = document.createElement('script')
    script.src = "https://apis.google.com/js/api.js"

    script.onload = () => {
      window.gapi.load('client', () => {
        window.gapi.client.init(apiObj)
          .then(() => {
            resolve(script)
            console.log('API is ready')
          })
          .catch(() => console.log('API not loaded. There is an error'))
      })
    }

    script.onerror = () => reject(new Error("Script load error:"));

    document.body.appendChild(script)
  });
}



/* Iniciamos la app secuencialmente */

(async () => {
  await loadGoogleApi()

  store.firebaseAuthIsReady.then(async () => {
    ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'))
  })
})()




// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
