import React, { Component } from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import { MuiThemeProvider } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import theme from './graphics/theme'
import themeHooks from './graphics/themeHooks'
import GlobalStyles from './things/GlobalStyles'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import './UI/material-dashboard-pro-react/src/assets/scss/material-dashboard-pro-react.scss'
import { HandleProgressBar } from './components/UI/Progress'
import { HandleAlert } from './components/UI/Alert'
import Nav from './components/nav/Nav'
import Dashboard from './components/layout/admin/Dashboard'
import Login from './components/auth/Login'
import LoginAntiguo from './components/auth/loginPrimitivo'
import AutoSignIn from './components/auth/AutoSignIn'
import NuevoUsuario from './components/auth/NuevoUsuario'
import Wizard from './components/auth/WizardClientes/Wizard'
import NuevoCliente from './components/auth/NuevoCliente'
// Super Calendar
import SuperCal from './components/calendar/SuperCal'
import SuperSecret from './components/secret/SuperSecret'
import InvitarEspecialista from './components/users/admin/InvitarEspecialista'
import NuevoEspecialista from './components/users/especialistas/NuevoEspecialista'
import Home from './components/Home'

const style = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  width: '100%',
  paddingTop: '1em'
}

//
//<Link to="/" className="logo"><img src={logo} alt="logo" /></Link>

class App extends Component {

  render() {

    return (
      <BrowserRouter>
        <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={themeHooks}>
          <CssBaseline />
          <GlobalStyles />
          <HandleProgressBar />
          <HandleAlert />

          <Nav />


          <main className="App" style={style}>

            <Switch>
              <Route exact path='/'component={Home} />
              <Route exact path='/login'component={Login} />
              <Route exact path='/signin'component={AutoSignIn} />
              <Route exact path='/login/forgot-password'component={Login} />
              <Route exact path='/loginAntiguo'component={LoginAntiguo} />
              <Route exact path='/nuevoUsuario'component={NuevoUsuario} />
              <Route exact path='/nuevo-cliente-old'component={Wizard} />
              <Route exact path='/nuevo-cliente'component={NuevoCliente} />

              <Route exact path='/clientes'component={SuperCal} />
              <Route exact path='/clientes/citas/:id'component={SuperCal} />
              <Route exact path='/clientes/citas/validar/:id'component={SuperCal} />

              <Route exact path='/especialistas'component={SuperCal} />
              <Route exact path='/especialistas/citas/:id'component={SuperCal} />
              <Route exact path='/especialistas/nuevo-especialista' component={NuevoEspecialista} />

              <Route exact path='/dashboard'component={Dashboard} />
              <Route exact path='/admin'component={SuperCal} />
              <Route exact path='/admin/citas/:id'component={SuperCal} />
              <Route exact path='/admin/invitar-especialista' component={InvitarEspecialista} />

              <Route exact path='/superSecret'component={SuperSecret} />
            </Switch>

          </main>
        </ThemeProvider>
        </MuiThemeProvider>
      </BrowserRouter>
    )
  }
}

export default App
