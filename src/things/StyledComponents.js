import styled from 'styled-components'

export const MiniImg = styled.img`
  max-width: 200px;
`
export const LogBox = styled.section`
  flex: 1;
  padding: 4em;
  & + & { border-left: 1px solid gainsboro; }
`
