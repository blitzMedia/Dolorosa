
export const startGapi = async () => {
  await window.gapi.client.init({
    apiKey: process.env.REACT_APP_GAPI_KEY,
    clientId: process.env.REACT_APP_GAPI_CLIENT_ID,
    discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
    scope: 'https://www.googleapis.com/auth/calendar'
  })
}
