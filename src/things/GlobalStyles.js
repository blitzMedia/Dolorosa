import { createGlobalStyle, css } from 'styled-components'

const GlobalStyles = createGlobalStyle`
  body {
    font-size: 16px !important;
  }
  body,
  body, h1, h2, h3, h4, h5, h6 {
    font-family: 'Product Sans' !important;
    font-weight: 300;
  }
  .full {
    height: 100vh;
    width: 100%;
  }
  .text-center {
    text-align: center
  }
  .vertical-distribute {
    * + * { margin-top: 2rem !important; }
  }

  .getItDown { margin-top: auto; }

  ${[5, 10, 15, 20, 40].map((size) => css`
    ${['top', 'right', 'bottom', 'left'].map((dir) => css`
      .m${dir[0]}-${size}{
        margin-${dir}: ${size}px;
      }
    `)}
  `)}

  @keyframes bounce {
    0%, 20%, 60%, 100% { transform: translateY(0); }
    40% { transform: translateY(-20px); }
    80% { transform: translateY(-10px); }
  }
  .bouncy { animation: bounce 1s infinite; }

  /**
    * simpleEntrance
    * - enter in simply
    */
   @keyframes simpleEntrance {
    0% {
      opacity: 0%;
      transform: translate3d(-2000px,500px,0) scale(10);
    }
    1% { opacity: 100%}
    40% { transform: translate3d(2000px,0px,0) scale(5);}
    50% { transform: translate3d(-2000px,-100px,0) scale(3);}
    60% { transform: translate3d(2000px,100px,0) scale(2);}
    70% { transform: translate3d(-2000px,400px,0) scale(.1);}
    75% { transform: translate3d(0px,0px,0) scale(.1);}
    80% { transform:  rotate(360deg) translateX(150px) rotate(-360deg)}
  }

  .simpleEntrance {
    animation-timing-function: linear;
    transform-origin: bottom center;
    animation-name: simpleEntrance;
    animation-duration: 10s;
    position: relative;
  }

  .simpleEntrance:before {
    content: '';
    position: absolute;
    left: 45%;
    top: 88%;
    background-image: url('http://www.stickpng.com/assets/images/584999a27b7d4d76317f5ffe.png');
    background-repeat: no-repeat;
    background-size: 100%;
    width: 80px;
    height: 200px;
    animation: nope 3s ease forwards;
    animation-delay: 12s;
    transform: scale(2.5);
    opacity: 0;
  }

  @keyframes nope {
    0% {
      opacity: 0;
      transform: scale(2.5) translateY(-200px);
    } 50% {
      opacity: 1;
    } 100% {
      transform: scale(2.5) translateY(0);
      opacity: 1;
    }
  }

  [class*="Navbar-appBar"] {
    position: sticky !important;
  }

  .capitalize { text-transform: capitalize }

`

export default GlobalStyles
